package com.example.chaminduchanaka.ramzijewelry.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.chaminduchanaka.ramzijewelry.R;
import com.example.chaminduchanaka.ramzijewelry.bean.Product;
import com.example.chaminduchanaka.ramzijewelry.bean.ReOpen;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ReOpenAdapter extends BaseAdapter {

    private ArrayList<ReOpen> list;
    private Activity context;
    private LayoutInflater inflater;

    public ReOpenAdapter(ArrayList<ReOpen> list, Activity context) {
        this.list = list;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ReOpenAdapter.ViewHolder holder = null;
        int type = getItemViewType(position);
        if (convertView == null) {
            holder = new ReOpenAdapter.ViewHolder();

            convertView = inflater.inflate(R.layout.model_re_open_list, null);
            holder.tv_id = (TextView) convertView.findViewById(R.id.tv_id);
            holder.tv_date = (TextView) convertView.findViewById(R.id.tv_date);

            convertView.setTag(holder);
        } else {
            holder = (ReOpenAdapter.ViewHolder) convertView.getTag();
        }

        holder.tv_id.setText( list.get(position).getId());
        holder.tv_date.setText( list.get(position).getDate());

        return convertView;
    }

    public static class ViewHolder {
        public TextView tv_id;
        public TextView tv_date;
    }
}
