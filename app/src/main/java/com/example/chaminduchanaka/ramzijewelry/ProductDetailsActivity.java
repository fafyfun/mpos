package com.example.chaminduchanaka.ramzijewelry;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.ramzijewelry.bean.Item;
import com.example.chaminduchanaka.ramzijewelry.service.GlobalVariables;
import com.example.chaminduchanaka.ramzijewelry.service.SaveData;
import com.example.chaminduchanaka.ramzijewelry.service.UsefullData;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.chaminduchanaka.ramzijewelry.service.Constance.ADD_STOCK;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.CHECK_STOCK;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.DISCOUNT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.PASSWORD;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.SCAN_PRODUCT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_ID;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_NAME;

public class ProductDetailsActivity extends AppCompatActivity {
    private SaveData saveData;
    private RequestQueue requestQueue;
    private UsefullData usefullData;

    private AVLoadingIndicatorView avi;

    private LinearLayout ll_header;
    private LinearLayout ll_description;
    private LinearLayout ll_availabiity;
    private LinearLayout ll_product_no;
    private LinearLayout ll_price;
    private LinearLayout ll_length;
    private LinearLayout ll_etal;
    private LinearLayout ll_gross;
//    private LinearLayout ll_images;

    private Button btn_process;
    private Button btn_menu;
    private Button btn_back;

    private ImageView iv_logo;
    private ImageView iv_mpos_logo;
    private ImageView iv_pic_1;
    private ImageView iv_pic_2;
    private ImageView iv_pic_3;
    private ImageView iv_pic_4;
    private ImageView iv_pic_5;

    private TextView tv_product_number;
    //    private TextView tv_product_name;
    private TextView tv_product_price;
    private TextView tv_description;
    private TextView tv_availability;
    private TextView tv_cart_items;
    private TextView tv_length;
    private TextView tv_metal_type;
    private TextView tv_gross_weight;
    private TextView tv_design_code;
    private TextView tv_certificate_num;

    private RelativeLayout rl_log_out;
    private RelativeLayout rl_bill;

    private String product_number;
    private String price;
    private String discount;

    Item obj = new Item();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        saveData = new SaveData(this);
        requestQueue = Volley.newRequestQueue(this);
        usefullData = new UsefullData(this);

        product_number = getIntent().getStringExtra("number");

        avi = findViewById(R.id.avi);
        iv_logo = findViewById(R.id.iv_logo);
//        iv_pic_1 = findViewById(R.id.iv_pic_1);
//        iv_pic_2 = findViewById(R.id.iv_pic_2);
//        iv_pic_3 = findViewById(R.id.iv_pic_3);
//        iv_pic_4 = findViewById(R.id.iv_pic_4);
//        iv_pic_5 = findViewById(R.id.iv_pic_5);
        iv_mpos_logo = findViewById(R.id.iv_mpos_logo);
        tv_product_number = findViewById(R.id.tv_product_number);
//        tv_product_name = findViewById(R.id.tv_product_name);
        tv_product_price = findViewById(R.id.tv_product_price);
        tv_description = findViewById(R.id.tv_description);
        tv_availability = findViewById(R.id.tv_availability);
        tv_cart_items = findViewById(R.id.tv_cart_items);
        tv_length = findViewById(R.id.tv_length);
        tv_metal_type = findViewById(R.id.tv_metal_type);
        tv_gross_weight = findViewById(R.id.tv_gross_weight);
        tv_design_code = findViewById(R.id.tv_design_code);
        tv_certificate_num = findViewById(R.id.tv_certificate_num);

        ll_header = findViewById(R.id.ll_header);
        ll_description = findViewById(R.id.ll_description);
        ll_availabiity = findViewById(R.id.ll_availabiity);
//        ll_images = findViewById(R.id.ll_images);

        rl_log_out = findViewById(R.id.rl_log_out);
        rl_bill = findViewById(R.id.rl_bill);

        btn_process = findViewById(R.id.btn_process);
        btn_menu = findViewById(R.id.btn_menu);
        btn_back = findViewById(R.id.btn_back);

        btn_process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean has_product = false;

                Log.e("AL Size", "" + GlobalVariables.al_items.size());

                if (GlobalVariables.al_items.size() > 0) {
                    for (int i = 0; i < GlobalVariables.al_items.size(); i++) {
                        if (GlobalVariables.al_items.get(i).getId().equals(obj.getId())) {
                            //set new quantity
                            int qty = Integer.parseInt(GlobalVariables.al_items.get(i).getQty()) + 1;
                            obj.setQty(Integer.toString(qty));

                            //set new discount
                            double new_discount = Double.parseDouble(discount);
                            obj.setDiscount(Double.toString(new_discount));

                            //set new price total of prices
                            int tot_price = Integer.parseInt(price) * qty;
                            obj.setPrice(Integer.toString(tot_price));

                            //set new discount total
                            double total_discount = 0;
                            if (!discount.equals("0")) {
                                total_discount = (tot_price * new_discount) / 100;
                            }
                            obj.setDiscount_total(Double.toString(total_discount));

                            //set new sub total
                            double new_sub_total = tot_price - total_discount;
                            obj.setSub_total(Double.toString(new_sub_total));

                            GlobalVariables.al_items.remove(i);

                            GlobalVariables.al_items.add(obj);

                            has_product = true;

                            break;
                        }
                    }
                }

                if (!has_product) {
                    GlobalVariables.al_items.add(obj);
                }

//                addStock();

                Intent intent = new Intent(ProductDetailsActivity.this, BillSummary.class);
                startActivity(intent);
            }
        });

        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductDetailsActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        rl_log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });

        iv_mpos_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductDetailsActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rl_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductDetailsActivity.this, BillSummary.class);
                startActivity(intent);
            }
        });

        loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GlobalVariables.al_items.size() > 0) {
            tv_cart_items.setVisibility(View.VISIBLE);
            tv_cart_items.setText(Integer.toString(GlobalVariables.al_items.size()));
        } else {
            tv_cart_items.setVisibility(View.GONE);
        }

        checkStock();
    }

    private void loadData() {
        String url = BASE_URL + SCAN_PRODUCT;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hide loader and visible fields
                        avi.hide();
                        ll_header.setVisibility(View.VISIBLE);
                        ll_description.setVisibility(View.VISIBLE);
//                        ll_images.setVisibility(View.VISIBLE);

                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                //get product details
                                JSONArray jsonArray = jsonObject.getJSONArray("product");
                                JSONObject product = jsonArray.getJSONObject(0);

//                                tv_stock.setVisibility(View.VISIBLE);

                                tv_product_number.setText(product_number);
                                obj.setId(product_number);

                                if (product.has("product_name")) {
                                    String product_name = product.getString("product_name");
//                                    tv_product_name.setText(product_name);
                                    obj.setName(product_name);
                                }

                                if (product.has("product_price")) {
                                    price = product.getString("product_price");
                                    tv_product_price.setText("Rs. " + price + "/=");
                                    obj.setPrice(price);

                                    obj.setItem_price(price);
                                }

                                Log.e("Discount", saveData.retreiveString(DISCOUNT));
                                if (!saveData.retreiveString(DISCOUNT).equals("")) {
                                    discount = saveData.retreiveString(DISCOUNT);
                                    obj.setDiscount(discount);
                                } else {
                                    if (product.has("discount")) {
                                        discount = product.getString("discount");
                                        obj.setDiscount(discount);
                                    }
                                }

                                if (product.has("description")) {
                                    String description = product.getString("description");
                                    tv_description.setText(description);
                                }

                                if (product.has("image")) {
                                    String image = product.getString("image");
                                    Picasso.get().load(image).into(iv_logo);
                                }

                                if (product.has("length")) {
                                    String size = product.getString("length");
                                    tv_length.setText(size);
                                }

                                if (product.has("weight")) {
                                    String color = product.getString("weight");
                                    tv_gross_weight.setText(color);
                                }

//                                if (product.has("image1")) {
//                                    String image = product.getString("image1");
//                                    Picasso.get().load(image).into(iv_pic_1);
//                                } else {
//                                    iv_pic_1.setVisibility(View.GONE);
//                                }
//
//                                if (product.has("image2")) {
//                                    String image = product.getString("image2");
//                                    Picasso.get().load(image).into(iv_pic_2);
//                                } else {
//                                    iv_pic_2.setVisibility(View.GONE);
//                                }
//
//                                if (product.has("image3")) {
//                                    String image = product.getString("image3");
//                                    Picasso.get().load(image).into(iv_pic_3);
//                                } else {
//                                    iv_pic_3.setVisibility(View.GONE);
//                                }
//
//                                if (product.has("image4")) {
//                                    String image = product.getString("image4");
//                                    Picasso.get().load(image).into(iv_pic_4);
//                                } else {
//                                    iv_pic_4.setVisibility(View.GONE);
//                                }
//
//                                if (product.has("image5")) {
//                                    String image = product.getString("image5");
//                                    Picasso.get().load(image).into(iv_pic_5);
//                                } else {
//                                    iv_pic_5.setVisibility(View.GONE);
//                                }

                                //calculate dicount total
                                double discount_total = 0;
                                if (!discount.equals("0")) {
                                    discount_total = (Integer.parseInt(price) * Integer.parseInt(discount)) / 100;
                                }
                                obj.setDiscount_total(Double.toString(discount_total));

                                //calculate sub total
                                int int_price = Integer.parseInt(price);
                                double sub_total = int_price - discount_total;
                                obj.setSub_total(Double.toString(sub_total));

                                //set quantity
                                obj.setQty("1");

                                if (product.has("metal_type")) {
                                    String shape = product.getString("metal_type");
                                    tv_metal_type.setText(shape);
                                }

                                if (product.has("design_code")) {
                                    String code = product.getString("design_code");
                                    tv_design_code.setText(code);
                                }

                                if (product.has("gem_cert_no")) {
                                    String code = product.getString("gem_cert_no");
                                    tv_certificate_num.setText(code);
                                }

                            } else {
                                String message = jsonObject.getString("message");
                                usefullData.displayMessage(message);
                            }
                        } catch (JSONException e) {
                            Log.e("checkLogin()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        avi.hide();

                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", product_number);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void logout() {
        saveData.saveString(USER_NAME, "");
        saveData.saveString(PASSWORD, "");
        saveData.saveString(DISCOUNT, "");

        GlobalVariables.al_items.clear();
        GlobalVariables.sub_total = "";
        GlobalVariables.total_discount = "";
        GlobalVariables.total_price = "";
        GlobalVariables.loyalty_discount_rate = "";
        GlobalVariables.loyalty_discount = "";
        GlobalVariables.vat = "";
        GlobalVariables.total_after_vat = "";
        GlobalVariables.net_price = "";

        Intent loginscreen = new Intent(this, LoginActivity.class);
        loginscreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginscreen);
        this.finish();
    }

    private void addStock() {
        String url = BASE_URL + ADD_STOCK;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Add Stock Response", response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        avi.hide();

                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", product_number);
                params.put("price", price);
                params.put("size", "null");
                params.put("stock", "-1");
                params.put("user", saveData.retreiveString(USER_ID));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void checkStock() {
        String url = BASE_URL + CHECK_STOCK;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Check Stock Response", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                //get product details
                                JSONArray jsonArray = jsonObject.getJSONArray("product_stock");
                                JSONObject product = jsonArray.getJSONObject(0);

                                if (product.has("stock")) {
                                    int stock = Integer.parseInt(product.getString("stock"));

                                    Log.e("Stock", "" + stock);

                                    ll_availabiity.setVisibility(View.VISIBLE);

                                    if (stock == 0) {
                                        btn_process.setEnabled(false);
                                        tv_availability.setText("Not Available");
                                        tv_availability.setBackgroundColor(Color.RED);
                                    }
                                }

                            } else {
                                String message = jsonObject.getString("message");
                                usefullData.displayMessage(message);
                            }
                        } catch (JSONException e) {
                            Log.e("checkLogin()", "Json Error");
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        avi.hide();

//                        if (error instanceof NetworkError) {
//                            usefullData.displayMessage("Network error occured..!");
//                        } else if (error instanceof ServerError) {
//                            usefullData.displayMessage("Network error occured..!");
//                        } else if (error instanceof AuthFailureError) {
//                            usefullData.displayMessage("Authentication failure..!");
//                        } else if (error instanceof NoConnectionError) {
//                            usefullData.displayMessage("No internet connection..!");
//                        } else {
//                            usefullData.displayMessage("Unknown error occured..!");
//                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", product_number);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
