package com.example.chaminduchanaka.ramzijewelry.service;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.MY_PREFS_NAME;

public class SaveData {
    Context context;

    public SaveData(Context context) {
        this.context = context;
    }

    public void saveString(String key, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String retreiveString(String key) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString(key, "");
        return  restoredText;
    }
}
