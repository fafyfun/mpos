package com.example.chaminduchanaka.ramzijewelry;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.ramzijewelry.adapter.CardDetails;
import com.example.chaminduchanaka.ramzijewelry.service.GlobalVariables;
import com.example.chaminduchanaka.ramzijewelry.service.SaveData;
import com.example.chaminduchanaka.ramzijewelry.service.UsefullData;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.example.chaminduchanaka.ramzijewelry.service.Constance.ADD_ORDER;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.DISCOUNT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.PASSWORD;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.TAX;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_ID;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_NAME;

public class ChequePaymentActivity extends AppCompatActivity {
    private Button btn_menu;
    private Button btn_back;
    private Button btn_process;

    private ImageView iv_logo;

    private TextView tv_sub_total;
    private TextView tv_total_discount;
    private TextView tv_total_price;
    private TextView tv_vat;
    private TextView tv_net_price;
    private TextView tv_cart_items;
    private TextView tv_vat_lbl;
    private TextView tv_loyalty;
    private TextView tv_loyalty_lbl;

    private EditText et_date;
    private EditText et_bank_name;
    private EditText et_cheque_number;

    private LinearLayout ll_loyalty;

    private RelativeLayout rl_bill;
    private RelativeLayout rl_logout;

    private SaveData saveData;
    private UsefullData usefullData;
    private RequestQueue requestQueue;

    private ArrayList<CardDetails> al_card_details;

    Calendar myCalendar = Calendar.getInstance();

    private String cus_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheque_payment);

        saveData = new SaveData(this);
        usefullData = new UsefullData(this);
        requestQueue = Volley.newRequestQueue(this);

        btn_menu = findViewById(R.id.btn_menu);
        btn_back = findViewById(R.id.btn_back);
        btn_process = findViewById(R.id.btn_process);

        tv_sub_total = findViewById(R.id.tv_sub_total);
        tv_total_discount = findViewById(R.id.tv_total_discount);
        tv_total_price = findViewById(R.id.tv_total_price);
        tv_vat = findViewById(R.id.tv_vat);
        tv_net_price = findViewById(R.id.tv_net_price);
        tv_cart_items = findViewById(R.id.tv_cart_items);
        tv_vat_lbl = findViewById(R.id.tv_vat_lbl);
        tv_loyalty = findViewById(R.id.tv_loyalty);
        tv_loyalty_lbl = findViewById(R.id.tv_loyalty_lbl);

        et_date = findViewById(R.id.et_date);
        et_bank_name = findViewById(R.id.et_bank_name);
        et_cheque_number = findViewById(R.id.et_cheque_number);

        ll_loyalty = findViewById(R.id.ll_loyalty);

        rl_bill = findViewById(R.id.rl_bill);
        rl_logout = findViewById(R.id.rl_log_out);

        iv_logo = findViewById(R.id.iv_logo);

        tv_vat_lbl.setText("TAX  " + saveData.retreiveString(TAX) + "%");

        cus_id = getIntent().getStringExtra("cus_id");

        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChequePaymentActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        iv_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChequePaymentActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_bank_name.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Bank name is required");
                } else if (et_cheque_number.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Cheque number is required");
                } else if (et_date.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Date is required");
                } else {
                    usefullData.showProgress("Please wait...");

                    //get card details
                    al_card_details = new ArrayList<>();
                    CardDetails obj = new CardDetails();
                    obj.setName(et_bank_name.getText().toString().trim());
                    obj.setType(et_date.getText().toString().trim());
                    obj.setNumber(et_cheque_number.getText().toString().trim());
                    obj.setAmount(GlobalVariables.net_price);
                    al_card_details.add(obj);

                    saveData();
                }
            }
        });

        rl_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChequePaymentActivity.this, BillSummary.class);
                startActivity(intent);
            }
        });

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "yyyy/MM/dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
                et_date.setText(sdf.format(myCalendar.getTime()));
            }

        };

        et_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(ChequePaymentActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        rl_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData.saveString(USER_NAME, "");
                saveData.saveString(PASSWORD, "");
                saveData.saveString(DISCOUNT, "");

                GlobalVariables.al_items.clear();
                GlobalVariables.sub_total = "";
                GlobalVariables.total_discount = "";
                GlobalVariables.total_price = "";
                GlobalVariables.loyalty_discount_rate = "";
                GlobalVariables.loyalty_discount = "";
                GlobalVariables.vat = "";
                GlobalVariables.total_after_vat = "";
                GlobalVariables.net_price = "";

                Intent loginscreen=new Intent(ChequePaymentActivity.this, LoginActivity.class);
                loginscreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginscreen);
                finish();
            }
        });

        setData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GlobalVariables.al_items.size() > 0) {
            tv_cart_items.setVisibility(View.VISIBLE);
            tv_cart_items.setText(Integer.toString(GlobalVariables.al_items.size()));
        } else {
            tv_cart_items.setVisibility(View.GONE);
        }
    }

    private void setData() {
        tv_sub_total.setText(GlobalVariables.sub_total);
        tv_total_discount.setText(GlobalVariables.total_discount);
        tv_total_price.setText(GlobalVariables.total_price);
        tv_vat.setText(GlobalVariables.total_after_vat);
        tv_net_price.setText(GlobalVariables.net_price);

        if (!GlobalVariables.loyalty_discount.equals("")) {
            ll_loyalty.setVisibility(View.VISIBLE);
            tv_loyalty_lbl.setText("Loyalty  " + GlobalVariables.loyalty_discount_rate + "%");
            tv_loyalty.setText(GlobalVariables.loyalty_discount);
        }
    }

    private void saveData() {
        String url = BASE_URL + ADD_ORDER;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        usefullData.dismissProgress();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");

                            if (success.equals("1")) {
                                //clear arraylist
                                GlobalVariables.al_items.clear();

                                usefullData.showOrderSuccessfullDialog();

                                Log.e("Al size", "" + GlobalVariables.al_items.size());
                            }
                            usefullData.displayMessage(message);
                        } catch (JSONException e) {
                            Log.e("Add order", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        usefullData.dismissProgress();

                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }

                        Log.e("Error", error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Gson gson = new Gson();

                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", saveData.retreiveString(USER_ID));
                params.put("items", gson.toJson(GlobalVariables.al_items));
                params.put("tax", GlobalVariables.vat);
                params.put("method", "cheque");
                params.put("card_details", gson.toJson(al_card_details));
                params.put("paid", GlobalVariables.net_price);
                params.put("balance", "null");
                params.put("cus_id", cus_id);
                params.put("price", GlobalVariables.net_price);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
