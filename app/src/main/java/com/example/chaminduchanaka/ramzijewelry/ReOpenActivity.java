package com.example.chaminduchanaka.ramzijewelry;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.ramzijewelry.adapter.ReOpenAdapter;
import com.example.chaminduchanaka.ramzijewelry.bean.Item;
import com.example.chaminduchanaka.ramzijewelry.bean.ReOpen;
import com.example.chaminduchanaka.ramzijewelry.service.GlobalVariables;
import com.example.chaminduchanaka.ramzijewelry.service.SaveData;
import com.example.chaminduchanaka.ramzijewelry.service.UsefullData;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.chaminduchanaka.ramzijewelry.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.DISCOUNT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.HOLD;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.PASSWORD;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.RE_OPEN;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.RE_OPEN_SELECT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_ID;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_NAME;

public class ReOpenActivity extends AppCompatActivity {

    private RequestQueue requestQueue;
    private UsefullData usefullData;
    private SaveData saveData;

    private Button btn_back;
    private Button btn_menu;

    private RelativeLayout rl_bill;
    private RelativeLayout rl_logout;

    private TextView tv_cart_items;

    private ListView lv_re_open;

    private ArrayList<ReOpen> al_re_open;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_re_open);

        requestQueue = Volley.newRequestQueue(this);
        usefullData = new UsefullData(this);
        saveData = new SaveData(this);

        rl_bill = findViewById(R.id.rl_bill);
        rl_logout = findViewById(R.id.rl_log_out);

        btn_back = findViewById(R.id.btn_back);
        btn_menu = findViewById(R.id.btn_menu);

        tv_cart_items = findViewById(R.id.tv_cart_items);

        lv_re_open = findViewById(R.id.lv_re_open);

        rl_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReOpenActivity.this, BillSummary.class);
                startActivity(intent);
            }
        });

        lv_re_open.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                usefullData.showProgress("Please wait...");
                selectReOpen(al_re_open.get(position).getId());
            }
        });

        rl_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData.saveString(USER_NAME, "");
                saveData.saveString(PASSWORD, "");
                saveData.saveString(DISCOUNT, "");

                GlobalVariables.al_items.clear();
                GlobalVariables.sub_total = "";
                GlobalVariables.total_discount = "";
                GlobalVariables.total_price = "";
                GlobalVariables.loyalty_discount_rate = "";
                GlobalVariables.loyalty_discount = "";
                GlobalVariables.vat = "";
                GlobalVariables.total_after_vat = "";
                GlobalVariables.net_price = "";

                Intent loginscreen=new Intent(ReOpenActivity.this, LoginActivity.class);
                loginscreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginscreen);
                finish();
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReOpenActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReOpenActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        usefullData.showProgress("Please wait...");
        getHoldItems();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GlobalVariables.al_items.size() > 0) {
            tv_cart_items.setVisibility(View.VISIBLE);
            tv_cart_items.setText(Integer.toString(GlobalVariables.al_items.size()));
        } else {
            tv_cart_items.setVisibility(View.GONE);
        }
    }

    private void getHoldItems() {
        al_re_open = new ArrayList<>();

        String url = BASE_URL + RE_OPEN;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        usefullData.dismissProgress();

                        Log.e("Response", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");

                            if (success.equals("1")) {

                                JSONArray jsonArray_product = jsonObject.getJSONArray("product");

                                for (int i = 0; i < jsonArray_product.length(); i++) {
                                    JSONObject jsonObject_product = jsonArray_product.getJSONObject(i);

                                    ReOpen obj = new ReOpen();

                                    if (jsonObject_product.has("id")) {
                                        obj.setId(jsonObject_product.getString("id"));
                                    }

                                    if (jsonObject_product.has("created")) {
                                        obj.setDate(jsonObject_product.getString("created"));
                                    }

                                    al_re_open.add(obj);
                                }

                                ReOpenAdapter adapter = new ReOpenAdapter(al_re_open, ReOpenActivity.this);
                                lv_re_open.setAdapter(adapter);

                            } else {
                                String message = jsonObject.getString("message");
                                usefullData.displayMessage(message);
                            }
                        } catch (JSONException e) {
                            Log.e("Add order", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        usefullData.dismissProgress();

                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("id", saveData.retreiveString(USER_ID));

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void selectReOpen(final String id) {
        String url = BASE_URL + RE_OPEN_SELECT;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        usefullData.dismissProgress();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");

                            if (success.equals("1")) {

                                JSONArray jsonArray_product = jsonObject.getJSONArray("product");

                                for (int i = 0; i < jsonArray_product.length(); i++) {
                                    JSONObject jsonObject_product = jsonArray_product.getJSONObject(i);

                                    if (jsonObject_product.has("items")) {
                                        JSONArray jsonArray = jsonObject_product.getJSONArray("items");
                                        for (int j = 0; j < jsonArray.length(); j++) {
                                            JSONObject object = jsonArray.getJSONObject(j);

                                            Item item = new Item();

                                            item.setId(object.getString("id"));
                                            item.setName(object.getString("name"));
                                            item.setItem_price(object.getString("unit_price"));
                                            item.setQty(object.getString("qty"));
                                            item.setDiscount(object.getString("discount"));

                                            //calculate price
                                            double price = Double.parseDouble(object.getString("unit_price")) * Double.parseDouble(object.getString("qty"));
                                            item.setPrice(usefullData.formatNumber(price));

                                            //discount total
                                            double discount_total = (price * Double.parseDouble(object.getString("discount"))) / 100;
                                            item.setDiscount_total(Double.toString(discount_total));

                                            //sub total
                                            double sub_total = price - discount_total;
                                            item.setSub_total(usefullData.formatNumber(sub_total));

                                            GlobalVariables.al_items.add(item);
                                        }

                                        Intent intent = new Intent(ReOpenActivity.this, BillSummary.class);
                                        startActivity(intent);
                                        finish();
                                    }

                                }

                            } else {
                                String message = jsonObject.getString("message");
                                usefullData.displayMessage(message);
                            }
                        } catch (JSONException e) {
                            Log.e("Add order", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        usefullData.dismissProgress();

                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("id", id);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
