package com.example.chaminduchanaka.ramzijewelry.service;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.example.chaminduchanaka.ramzijewelry.BillSummary;
import com.example.chaminduchanaka.ramzijewelry.DashboardActivity;
import com.example.chaminduchanaka.ramzijewelry.LoginActivity;
import com.example.chaminduchanaka.ramzijewelry.R;

import java.text.DecimalFormat;

import static com.example.chaminduchanaka.ramzijewelry.service.Constance.DISCOUNT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.PASSWORD;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_NAME;

public class UsefullData {
    Context context;
    ProgressDialog progressDialog;
    SaveData saveData;

    public UsefullData(Context context) {
        this.context = context;
        progressDialog = new ProgressDialog(context);
        saveData = new SaveData(context);
    }

    public void displayMessage(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public String formatNumber(double value) {
        DecimalFormat format = new DecimalFormat("#.00");
        return format.format(value);
    }

    public void showProgress(String msg) {
        progressDialog.setMessage(msg);
        progressDialog.show();
    }

    public void dismissProgress() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void showOrderSuccessfullDialog() {
        final Dialog dialog = new Dialog(context, R.style.dialog_theme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.order_successfully_dialog);

        Button btn_done = dialog.findViewById(R.id.btn_done);

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //logout
                saveData.saveString(USER_NAME, "");
                saveData.saveString(PASSWORD, "");
                saveData.saveString(DISCOUNT, "");

                Intent loginscreen=new Intent(context, LoginActivity.class);
                loginscreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(loginscreen);

                dialog.dismiss();
            }
        });

        dialog.show();

        //hide after 20 seconds
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (dialog.isShowing()) {
                    //logout
                    saveData.saveString(USER_NAME, "");
                    saveData.saveString(PASSWORD, "");
                    saveData.saveString(DISCOUNT, "");

                    Intent loginscreen=new Intent(context, LoginActivity.class);
                    loginscreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(loginscreen);

                    dialog.dismiss();
                }
            }
        };

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacks(runnable);
            }
        });

        handler.postDelayed(runnable, 20000);
    }
}
