package com.example.chaminduchanaka.ramzijewelry;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.ramzijewelry.adapter.ProductAdapter;
import com.example.chaminduchanaka.ramzijewelry.bean.Product;
import com.example.chaminduchanaka.ramzijewelry.service.GlobalVariables;
import com.example.chaminduchanaka.ramzijewelry.service.SaveData;
import com.example.chaminduchanaka.ramzijewelry.service.UsefullData;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.chaminduchanaka.ramzijewelry.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.DISCOUNT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.FULL_NAME;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.LOGIN;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.PASSWORD;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.PRODUCT_LIST;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_NAME;

public class ProductListActivity extends AppCompatActivity {
    private AVLoadingIndicatorView avi;
    private ListView lv_products;
    private EditText et_search;
    private ImageView iv_logo;

    private Button btn_back;
    private Button btn_menu;

    private TextView tv_cart_items;

    private RelativeLayout rl_bill;
    private RelativeLayout rl_logout;

    private RequestQueue requestQueue;
    private UsefullData usefullData;
    private SaveData saveData;

    private ArrayList<Product> al_product;
    private ArrayList<Product> al_product_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        requestQueue = Volley.newRequestQueue(this);
        usefullData = new UsefullData(this);
        saveData = new SaveData(this);

        lv_products = findViewById(R.id.lv_products);

        et_search = findViewById(R.id.et_search);

        avi = findViewById(R.id.avi);

        iv_logo = findViewById(R.id.iv_logo);

        tv_cart_items = findViewById(R.id.tv_cart_items);

        rl_bill = findViewById(R.id.rl_bill);
        rl_logout = findViewById(R.id.rl_log_out);

        btn_back = findViewById(R.id.btn_back);
        btn_menu = findViewById(R.id.btn_menu);

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                al_product_search = new ArrayList<>();
                if (et_search.getText().toString().trim().equals("")) {
                    ProductAdapter adapter = new ProductAdapter(al_product, ProductListActivity.this);
                    lv_products.setAdapter(adapter);
                } else {
                    for (int i = 0; i < al_product.size(); i++) {
                        if (al_product.get(i).getName().toLowerCase().contains(s.toString()) || al_product.get(i).getName().contains(s.toString())) {
                            al_product_search.add(al_product.get(i));
                        } else if (al_product.get(i).getId().contains(s)) {
                            al_product_search.add(al_product.get(i));
                        }
                    }
                    ProductAdapter adapter = new ProductAdapter(al_product_search, ProductListActivity.this);
                    lv_products.setAdapter(adapter);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lv_products.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //start product details activity
                Log.e("Serach size", "" + et_search.getText().toString().trim().length());
                if (et_search.getText().toString().trim().length() > 0) {
                    Intent intent = new Intent(ProductListActivity.this, ProductDetailsActivity.class);
                    intent.putExtra("number", al_product_search.get(position).getId());
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(ProductListActivity.this, ProductDetailsActivity.class);
                    intent.putExtra("number", al_product.get(position).getId());
                    startActivity(intent);
                }
            }
        });

        iv_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductListActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductListActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductListActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        rl_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductListActivity.this, BillSummary.class);
                startActivity(intent);
            }
        });

        rl_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData.saveString(USER_NAME, "");
                saveData.saveString(PASSWORD, "");
                saveData.saveString(DISCOUNT, "");

                GlobalVariables.al_items.clear();
                GlobalVariables.sub_total = "";
                GlobalVariables.total_discount = "";
                GlobalVariables.total_price = "";
                GlobalVariables.loyalty_discount_rate = "";
                GlobalVariables.loyalty_discount = "";
                GlobalVariables.vat = "";
                GlobalVariables.total_after_vat = "";
                GlobalVariables.net_price = "";

                Intent loginscreen=new Intent(ProductListActivity.this, LoginActivity.class);
                loginscreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginscreen);
                finish();
            }
        });

        getProductList();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (GlobalVariables.al_items.size() > 0) {
            tv_cart_items.setVisibility(View.VISIBLE);
            tv_cart_items.setText(Integer.toString(GlobalVariables.al_items.size()));
        } else {
            tv_cart_items.setVisibility(View.GONE);
        }
    }

    private void getProductList() {
        al_product = new ArrayList<>();

        String url = BASE_URL + PRODUCT_LIST;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        avi.hide();

                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                //get user details
                                JSONArray jsonArray = jsonObject.getJSONArray("products");
                                JSONArray jsonArray1 = jsonArray.getJSONArray(0);

                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject productJobj = jsonArray1.getJSONObject(i);
                                    Product obj = new Product();

                                    if (productJobj.has("id")) {
                                        obj.setId(productJobj.getString("id"));
                                    }

                                    if (productJobj.has("name")) {
                                        obj.setName(productJobj.getString("name"));
                                    }

                                    if (productJobj.has("price")) {
                                        obj.setPrice(productJobj.getString("price"));
                                    }

                                    if (productJobj.has("image")) {
                                        obj.setImage(productJobj.getString("image"));
                                    }

                                    al_product.add(obj);
                                }

                                ProductAdapter adapter = new ProductAdapter(al_product, ProductListActivity.this);
                                lv_products.setAdapter(adapter);

                            } else {
                                String message = jsonObject.getString("message");
                                usefullData.displayMessage(message);
                            }
                        } catch (JSONException e) {
                            Log.e("getProductList()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        avi.hide();

                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }
                    }
                }) {
        };
        requestQueue.add(stringRequest);
    }
}
