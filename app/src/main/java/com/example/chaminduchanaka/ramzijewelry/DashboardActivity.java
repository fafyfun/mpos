package com.example.chaminduchanaka.ramzijewelry;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bugsnag.android.Bugsnag;
import com.example.chaminduchanaka.ramzijewelry.service.GlobalVariables;
import com.example.chaminduchanaka.ramzijewelry.service.SaveData;

import static com.example.chaminduchanaka.ramzijewelry.service.Constance.DISCOUNT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.FULL_NAME;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.PASSWORD;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_NAME;

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener {
    private SaveData saveData;

    private LinearLayout ll_scan;
    private LinearLayout ll_menu;
    private LinearLayout ll_search;
    private RelativeLayout rl_logout;

    private TextView tv_welcome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        saveData = new SaveData(this);

        Bugsnag.init(this);

        ll_scan = findViewById(R.id.ll_scan);
        ll_scan.setOnClickListener(this);

        ll_menu = findViewById(R.id.ll_menu);
        ll_menu.setOnClickListener(this);

        rl_logout = findViewById(R.id.rl_logout);
        rl_logout.setOnClickListener(this);

        ll_search = findViewById(R.id.ll_search);
        ll_search.setOnClickListener(this);

        tv_welcome = findViewById(R.id.tv_welcome);

        if (!saveData.retreiveString(FULL_NAME).equals("")) {
            tv_welcome.setText("Welcome " + saveData.retreiveString(FULL_NAME));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_scan: {
                Intent intent = new Intent(DashboardActivity.this, ScanActivity.class);
                startActivity(intent);
                break;
            }

            case R.id.rl_logout: {
                saveData.saveString(USER_NAME, "");
                saveData.saveString(PASSWORD, "");
                saveData.saveString(DISCOUNT, "");

                GlobalVariables.al_items.clear();
                GlobalVariables.sub_total = "";
                GlobalVariables.total_discount = "";
                GlobalVariables.total_price = "";
                GlobalVariables.loyalty_discount_rate = "";
                GlobalVariables.loyalty_discount = "";
                GlobalVariables.vat = "";
                GlobalVariables.total_after_vat = "";
                GlobalVariables.net_price = "";

                Intent loginscreen=new Intent(this, LoginActivity.class);
                loginscreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginscreen);
                this.finish();

                break;
            }

            case R.id.ll_menu: {
                Intent intent = new Intent(this, MenuActivity.class);
                startActivity(intent);
                break;
            }

            case R.id.ll_search: {
                Intent intent = new Intent(this, ProductListActivity.class);
                startActivity(intent);
                break;
            }
        }
    }
}
