package com.example.chaminduchanaka.ramzijewelry;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.ramzijewelry.adapter.CardDetails;
import com.example.chaminduchanaka.ramzijewelry.service.GlobalVariables;
import com.example.chaminduchanaka.ramzijewelry.service.SaveData;
import com.example.chaminduchanaka.ramzijewelry.service.UsefullData;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fr.ganfra.materialspinner.MaterialSpinner;

import static com.example.chaminduchanaka.ramzijewelry.service.Constance.ADD_ORDER;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.DISCOUNT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.PASSWORD;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.TAX;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_ID;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_NAME;

public class MultiCardActivity extends AppCompatActivity {
    private Button btn_menu;
    private Button btn_back;
    private Button btn_process;
    private Button btn_add_another;

    private ImageView iv_logo;

    private TextView tv_sub_total;
    private TextView tv_total_discount;
    private TextView tv_total_price;
    private TextView tv_vat;
    private TextView tv_net_price;
    private TextView tv_cart_items;
    private TextView tv_net_total;
    private TextView tv_vat_lbl;
    private TextView tv_loyalty_lbl;
    private TextView tv_loyalty;

    private MaterialSpinner sp_card_type_1;
    private MaterialSpinner sp_card_type_2;

    private EditText et_holder_1;
    private EditText et_holder_2;
    private EditText et_card_num_4_1;
    private EditText et_card_num_4_2;
    private EditText et_card_amount_1;
    private EditText et_card_amount_2;

    private LinearLayout ll_card2;
    private LinearLayout ll_loyalty;

    private RelativeLayout rl_bill;
    private RelativeLayout rl_logout;

    private SaveData saveData;
    private UsefullData usefullData;
    private RequestQueue requestQueue;

    private ArrayList<CardDetails> al_card_details;

    double balance;

    private String cus_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_card);

        saveData = new SaveData(this);
        usefullData = new UsefullData(this);
        requestQueue = Volley.newRequestQueue(this);

        btn_menu = findViewById(R.id.btn_menu);
        btn_back = findViewById(R.id.btn_back);
        btn_process = findViewById(R.id.btn_process);
        btn_add_another = findViewById(R.id.btn_add_another);

        tv_sub_total = findViewById(R.id.tv_sub_total);
        tv_total_discount = findViewById(R.id.tv_total_discount);
        tv_total_price = findViewById(R.id.tv_total_price);
        tv_vat = findViewById(R.id.tv_vat);
        tv_net_price = findViewById(R.id.tv_net_price);
        tv_cart_items = findViewById(R.id.tv_cart_items);
        tv_net_total = findViewById(R.id.tv_net_total);
        tv_vat_lbl = findViewById(R.id.tv_vat_lbl);
        tv_loyalty_lbl = findViewById(R.id.tv_loyalty_lbl);
        tv_loyalty = findViewById(R.id.tv_loyalty);

        sp_card_type_1 = findViewById(R.id.sp_card_type_1);
        sp_card_type_2 = findViewById(R.id.sp_card_type_2);

        et_holder_1 = findViewById(R.id.et_holder_1);
        et_holder_2 = findViewById(R.id.et_holder_2);
        et_card_num_4_1 = findViewById(R.id.et_card_num_4_1);
        et_card_num_4_2 = findViewById(R.id.et_card_num_4_2);
        et_card_amount_1 = findViewById(R.id.et_card_amount_1);
        et_card_amount_2 = findViewById(R.id.et_card_amount_2);

        rl_bill = findViewById(R.id.rl_bill);
        rl_logout = findViewById(R.id.rl_log_out);

        ll_card2 = findViewById(R.id.ll_card2);
        ll_loyalty = findViewById(R.id.ll_loyalty);

        iv_logo = findViewById(R.id.iv_logo);

        tv_vat_lbl.setText("TAX  " + saveData.retreiveString(TAX) + "%");

        cus_id = getIntent().getStringExtra("cus_id");

        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MultiCardActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        iv_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MultiCardActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sp_card_type_1.getSelectedItemPosition() == 0) {
                    usefullData.displayMessage("Select a card type");
                } else if (et_holder_1.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Card holder name is required");
                } else if (et_card_num_4_1.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Card number is required");
                } else if (et_card_amount_1.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Enter the card price");
                } else if (sp_card_type_2.getSelectedItemPosition() == 0) {
                    usefullData.displayMessage("Select a card type");
                } else if (et_holder_2.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Card holder name is required");
                } else if (et_card_num_4_2.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Card number is required");
                } else if (et_card_amount_2.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Enter the card price");
                } else if (Double.parseDouble(et_card_amount_2.getText().toString()) < balance) {
                    usefullData.displayMessage("Invalid amount");
                } else {
                    usefullData.showProgress("Please wait...");

                    al_card_details = new ArrayList<>();

                    //get card 1 details
                    CardDetails obj = new CardDetails();
                    obj.setName(et_holder_1.getText().toString().trim());
                    if (sp_card_type_1.getSelectedItemPosition() == 1) {
                        obj.setType("Visa");
                    } else {
                        obj.setType("Master");
                    }
                    obj.setNumber(et_card_num_4_1.getText().toString().trim());
                    obj.setAmount(et_card_amount_1.getText().toString().trim());
                    al_card_details.add(obj);


                    //get card 2 details
                    CardDetails obj2 = new CardDetails();
                    obj2.setName(et_holder_2.getText().toString().trim());
                    if (sp_card_type_2.getSelectedItemPosition() == 1) {
                        obj2.setType("Visa");
                    } else {
                        obj2.setType("Master");
                    }
                    obj2.setNumber(et_card_num_4_2.getText().toString().trim());
                    obj2.setAmount(et_card_amount_2.getText().toString().trim());
                    al_card_details.add(obj2);

                    saveData();
                }
            }
        });

        btn_add_another.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_card2.setVisibility(View.VISIBLE);
            }
        });

        rl_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MultiCardActivity.this, BillSummary.class);
                startActivity(intent);
            }
        });

        et_card_amount_1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (et_card_amount_1.getText().toString().length() > 0) {
                    double net_total = Double.parseDouble(GlobalVariables.net_price);
                    double card_amount_1 = Double.parseDouble(et_card_amount_1.getText().toString());

                    if (card_amount_1 < net_total) {
                        balance = net_total - card_amount_1;

                        tv_net_total.setText(usefullData.formatNumber(balance));
                        et_card_amount_2.setText(usefullData.formatNumber(balance));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        et_card_amount_2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (et_card_amount_2.getText().toString().length() > 0) {
                    double card_amount_2 = Double.parseDouble(et_card_amount_2.getText().toString());
                    if (card_amount_2 > balance) {
                        usefullData.displayMessage("Invalid amount");
                        et_card_amount_2.setText(usefullData.formatNumber(balance));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        rl_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData.saveString(USER_NAME, "");
                saveData.saveString(PASSWORD, "");
                saveData.saveString(DISCOUNT, "");

                GlobalVariables.al_items.clear();
                GlobalVariables.sub_total = "";
                GlobalVariables.total_discount = "";
                GlobalVariables.total_price = "";
                GlobalVariables.loyalty_discount_rate = "";
                GlobalVariables.loyalty_discount = "";
                GlobalVariables.vat = "";
                GlobalVariables.total_after_vat = "";
                GlobalVariables.net_price = "";

                Intent loginscreen=new Intent(MultiCardActivity.this, LoginActivity.class);
                loginscreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginscreen);
                finish();
            }
        });

        //set spinner values
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.card));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_card_type_1.setAdapter(adapter);
        sp_card_type_2.setAdapter(adapter);

        setData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GlobalVariables.al_items.size() > 0) {
            tv_cart_items.setVisibility(View.VISIBLE);
            tv_cart_items.setText(Integer.toString(GlobalVariables.al_items.size()));
        } else {
            tv_cart_items.setVisibility(View.GONE);
        }
    }

    private void setData() {
        tv_sub_total.setText(GlobalVariables.sub_total);
        tv_total_discount.setText(GlobalVariables.total_discount);
        tv_total_price.setText(GlobalVariables.total_price);
        tv_vat.setText(GlobalVariables.total_after_vat);
        tv_net_price.setText(GlobalVariables.net_price);

        if (!GlobalVariables.loyalty_discount.equals("")) {
            ll_loyalty.setVisibility(View.VISIBLE);
            tv_loyalty_lbl.setText("Loyalty  " + GlobalVariables.loyalty_discount_rate + "%");
            tv_loyalty.setText(GlobalVariables.loyalty_discount);
        }
    }

    private void saveData() {
        String url = BASE_URL + ADD_ORDER;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        usefullData.dismissProgress();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");

                            if (success.equals("1")) {
                                //clear arraylist
                                GlobalVariables.al_items.clear();

                                usefullData.showOrderSuccessfullDialog();

                                Log.e("Al size", "" + GlobalVariables.al_items.size());
                            }
                            usefullData.displayMessage(message);
                        } catch (JSONException e) {
                            Log.e("Add order", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        usefullData.dismissProgress();

                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }

                        Log.e("Error", error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Gson gson = new Gson();

                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", saveData.retreiveString(USER_ID));
                params.put("items", gson.toJson(GlobalVariables.al_items));
                params.put("tax", GlobalVariables.vat);
                params.put("method", "multi_card");
                params.put("card_details", gson.toJson(al_card_details));
                params.put("paid", GlobalVariables.net_price);
                params.put("balance", "null");
                params.put("cus_id", cus_id);
                params.put("price", GlobalVariables.net_price);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void clearFields() {
        sp_card_type_1.setSelection(0);
        et_holder_1.setText("");
        et_card_num_4_1.setText("");
        et_card_amount_1.setText("");

        sp_card_type_1.setSelection(0);
        et_holder_2.setText("");
        et_card_num_4_2.setText("");
        et_card_amount_2.setText("");

        tv_sub_total.setText("0.00");
        tv_total_discount.setText("0.00");
        tv_total_price.setText("0.00");
        tv_vat.setText("0.00");
        tv_net_price.setText("0.00");
    }
}
