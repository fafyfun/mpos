package com.example.chaminduchanaka.ramzijewelry;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.ramzijewelry.service.UsefullData;
import com.google.zxing.Result;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.Manifest.permission.CAMERA;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.LOGIN;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.SCAN_PRODUCT;

public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private UsefullData usefullData;
    private RequestQueue requestQueue;

    private static final int REQUEST_CAMERA = 1;
    private ZXingScannerView scannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);

        usefullData = new UsefullData(this);
        requestQueue = Volley.newRequestQueue(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                usefullData.displayMessage("Permission is Granted");
            } else {
                requestPermission();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ScanActivity.this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(ScanActivity.this, CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    private void onRequestPermissionResult(int requestCode, String permission[], int grantResult[]) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResult.length > 0) {
                    boolean cameraAccepted = grantResult[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted) {
                        usefullData.displayMessage("Permission Granted");
                    } else {
                        usefullData.displayMessage("Permission Denied");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                displayMessageAlert("You need to allow access for both permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{CAMERA}, REQUEST_CAMERA);
                                                }
                                            }
                                        });
                                return;
                            }
                        }
                    }
                }
                break;
        }
    }

    private void displayMessageAlert(String message, DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(ScanActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", listener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if (scannerView == null) {
                    scannerView = new ZXingScannerView(this);
                    setContentView(scannerView);
                }
                scannerView.setResultHandler(this);
                scannerView.startCamera();
            } else {
                requestPermission();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        scannerView.stopCamera();
    }

    @Override
    public void handleResult(final Result result) {
        String url = BASE_URL + SCAN_PRODUCT;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                //get product details
                                String product_number = "";

                                JSONArray jsonArray = jsonObject.getJSONArray("product");
                                JSONObject product = jsonArray.getJSONObject(0);

                                if (product.has("product_id")) {
                                    product_number = product.getString("product_id");
                                }

                                //start product details activity
                                Intent intent = new Intent(ScanActivity.this, ProductDetailsActivity.class);
                                intent.putExtra("number", product_number);
                                startActivity(intent);

                            } else {
                                String message = jsonObject.getString("message");
                                usefullData.displayMessage(message);

                                onResume();
                            }
                        } catch (JSONException e) {
                            Log.e("checkLogin()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }

                        Log.e("Error", error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", result.getText());
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
