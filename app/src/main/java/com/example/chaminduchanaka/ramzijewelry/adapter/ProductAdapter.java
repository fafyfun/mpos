package com.example.chaminduchanaka.ramzijewelry.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.chaminduchanaka.ramzijewelry.R;
import com.example.chaminduchanaka.ramzijewelry.bean.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductAdapter extends BaseAdapter {
    private ArrayList<Product> list;
    private Activity context;
    private LayoutInflater inflater;

    public ProductAdapter(ArrayList<Product> list, Activity context) {
        this.list = list;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ProductAdapter.ViewHolder holder = null;
        int type = getItemViewType(position);
        if (convertView == null) {
            holder = new ProductAdapter.ViewHolder();

            convertView = inflater.inflate(R.layout.model_products, null);
            holder.tv_pro_name = (TextView) convertView.findViewById(R.id.tv_pro_name);
            holder.tv_pro_id = (TextView) convertView.findViewById(R.id.tv_pro_id);
            holder.tv_price = (TextView) convertView.findViewById(R.id.tv_price);
            holder.iv_product = (ImageView) convertView.findViewById(R.id.iv_product);

            convertView.setTag(holder);
        } else {
            holder = (ProductAdapter.ViewHolder) convertView.getTag();
        }

        holder.tv_pro_name.setText(list.get(position).getName());
        Picasso.get().load(list.get(position).getImage()).into(holder.iv_product);
        holder.tv_pro_id.setText(list.get(position).getId());
        holder.tv_price.setText("Rs. " + list.get(position).getPrice() + "/=");

        return convertView;
    }

    public static class ViewHolder {
        private ImageView iv_product;
        public TextView tv_pro_name;
        public TextView tv_pro_id;
        public TextView tv_price;
    }
}
