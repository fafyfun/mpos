package com.example.chaminduchanaka.ramzijewelry.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.ramzijewelry.R;
import com.example.chaminduchanaka.ramzijewelry.bean.Item;
import com.example.chaminduchanaka.ramzijewelry.service.GlobalVariables;
import com.example.chaminduchanaka.ramzijewelry.service.SaveData;
import com.example.chaminduchanaka.ramzijewelry.service.UsefullData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.chaminduchanaka.ramzijewelry.service.Constance.ADD_STOCK;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_ID;

public class BillItemsAdapter extends BaseAdapter {
    private ArrayList<Item> list;
    private Activity context;
    private LayoutInflater inflater;
    private UsefullData usefullData;
    private RequestQueue requestQueue;
    private SaveData saveData;

    public BillItemsAdapter(ArrayList<Item> list, Activity context) {
        this.list = list;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        usefullData = new UsefullData(context);
        requestQueue = Volley.newRequestQueue(context);
        saveData = new SaveData(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        BillItemsAdapter.ViewHolder holder = null;
        int type = getItemViewType(position);
        if (convertView == null) {
            holder = new BillItemsAdapter.ViewHolder();

            convertView = inflater.inflate(R.layout.model_bill_item, null);
            holder.tv_id = (TextView) convertView.findViewById(R.id.tv_id);
            holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            holder.tv_qty = (TextView) convertView.findViewById(R.id.tv_qty);
            holder.tv_price = (TextView) convertView.findViewById(R.id.tv_price);
            holder.tv_discount = (TextView) convertView.findViewById(R.id.tv_discount);
            holder.tv_total = (TextView) convertView.findViewById(R.id.tv_total);
            holder.tv_sub_total = (TextView) convertView.findViewById(R.id.tv_sub_total);
            holder.iv_close = (ImageView) convertView.findViewById(R.id.iv_close);

            convertView.setTag(holder);
        } else {
            holder = (BillItemsAdapter.ViewHolder) convertView.getTag();
        }

        holder.tv_id.setText(list.get(position).getId());
        holder.tv_name.setText(list.get(position).getName());
        holder.tv_qty.setText(list.get(position).getQty());

        //set price
        holder.tv_price.setText(usefullData.formatNumber(Double.parseDouble(list.get(position).getPrice())));

        //set discount
        holder.tv_discount.setText(list.get(position).getDiscount() + "%");

        //set total
        if (list.get(position).getDiscount_total().equals("0") || list.get(position).getDiscount_total().equals("0.0")) {
            holder.tv_total.setText("0.00");
        } else {
            holder.tv_total.setText(usefullData.formatNumber(Double.parseDouble(list.get(position).getDiscount_total())));
        }

        //set sub total
        holder.tv_sub_total.setText(usefullData.formatNumber(Double.parseDouble(list.get(position).getSub_total())));

        holder.iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = GlobalVariables.al_items.get(position).getId();
                String qty = GlobalVariables.al_items.get(position).getQty();
                String price = GlobalVariables.al_items.get(position).getSub_total();

                Log.e("Quantity", ""+qty);

//                addStock(id, "+" + qty, price);

                GlobalVariables.al_items.remove(position);
                notifyDataSetChanged();
                context.recreate();
            }
        });

        return convertView;
    }

    public static class ViewHolder {
        public TextView tv_id;
        public TextView tv_name;
        public TextView tv_qty;
        public TextView tv_price;
        public TextView tv_discount;
        public TextView tv_total;
        public TextView tv_sub_total;
        public ImageView iv_close;
    }

    private void addStock(final String id, final String value, final String price) {
        String url = BASE_URL + ADD_STOCK;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Add Stock Response", response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", id);
                params.put("price", price);
                params.put("size", "null");
                params.put("stock", value);
                params.put("user", saveData.retreiveString(USER_ID));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
