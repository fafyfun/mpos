package com.example.chaminduchanaka.ramzijewelry;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.ramzijewelry.adapter.CardDetails;
import com.example.chaminduchanaka.ramzijewelry.service.GlobalVariables;
import com.example.chaminduchanaka.ramzijewelry.service.SaveData;
import com.example.chaminduchanaka.ramzijewelry.service.UsefullData;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fr.ganfra.materialspinner.MaterialSpinner;

import static com.example.chaminduchanaka.ramzijewelry.service.Constance.ADD_ORDER;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.DISCOUNT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.PASSWORD;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.TAX;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_ID;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_NAME;

public class CardPaymentActivity extends AppCompatActivity {
    private Button btn_menu;
    private Button btn_back;
    private Button btn_process;

    private ImageView iv_logo;

    private TextView tv_sub_total;
    private TextView tv_total_discount;
    private TextView tv_total_price;
    private TextView tv_vat;
    private TextView tv_net_price;
    private TextView tv_cart_items;
    private TextView tv_vat_lbl;
    private TextView tv_loyalty;
    private TextView tv_loyalty_lbl;

    private MaterialSpinner sp_card_type;

    private LinearLayout ll_loyalty;

    private EditText et_holder;
    private EditText et_card_num_1;
    private EditText et_card_num_2;
    private EditText et_card_num_3;
    private EditText et_card_num_4;

    private RelativeLayout rl_bill;
    private RelativeLayout rl_logout;

    private SaveData saveData;
    private UsefullData usefullData;
    private RequestQueue requestQueue;

    private ArrayList<CardDetails> al_card_details;

    private String cus_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_payment);

        saveData = new SaveData(this);
        usefullData = new UsefullData(this);
        requestQueue = Volley.newRequestQueue(this);

        btn_menu = findViewById(R.id.btn_menu);
        btn_back = findViewById(R.id.btn_back);
        btn_process = findViewById(R.id.btn_process);

        tv_sub_total = findViewById(R.id.tv_sub_total);
        tv_total_discount = findViewById(R.id.tv_total_discount);
        tv_total_price = findViewById(R.id.tv_total_price);
        tv_vat = findViewById(R.id.tv_vat);
        tv_net_price = findViewById(R.id.tv_net_price);
        tv_cart_items = findViewById(R.id.tv_cart_items);
        tv_vat_lbl = findViewById(R.id.tv_vat_lbl);
        tv_loyalty = findViewById(R.id.tv_loyalty);
        tv_loyalty_lbl = findViewById(R.id.tv_loyalty_lbl);

        sp_card_type = findViewById(R.id.sp_card_type);

        ll_loyalty = findViewById(R.id.ll_loyalty);

        et_holder = findViewById(R.id.et_holder);
        et_card_num_1 = findViewById(R.id.et_card_num_1);
        et_card_num_2 = findViewById(R.id.et_card_num_2);
        et_card_num_3 = findViewById(R.id.et_card_num_3);
        et_card_num_4 = findViewById(R.id.et_card_num_4);

        rl_bill = findViewById(R.id.rl_bill);
        rl_logout = findViewById(R.id.rl_log_out);

        iv_logo = findViewById(R.id.iv_logo);

        cus_id = getIntent().getStringExtra("cus_id");

        tv_vat_lbl.setText("TAX  " + saveData.retreiveString(TAX) + "%");

        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CardPaymentActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        iv_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CardPaymentActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sp_card_type.getSelectedItemPosition() == 0) {
                    usefullData.displayMessage("Select a card type");
                } else if (et_holder.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Card holder name is required");
                } else if (et_card_num_4.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Card number is required");
                } else {
                    usefullData.showProgress("Please wait...");

                    //get card details
                    al_card_details = new ArrayList<>();
                    CardDetails obj = new CardDetails();
                    obj.setName(et_holder.getText().toString().trim());
                    if (sp_card_type.getSelectedItemPosition() == 1) {
                        obj.setType("Visa");
                    } else {
                        obj.setType("Master");
                    }
                    obj.setNumber(et_card_num_4.getText().toString().trim());
                    obj.setAmount(GlobalVariables.net_price);
                    al_card_details.add(obj);

                    saveData();
                }
            }
        });

        rl_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CardPaymentActivity.this, BillSummary.class);
                startActivity(intent);
            }
        });

        rl_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData.saveString(USER_NAME, "");
                saveData.saveString(PASSWORD, "");
                saveData.saveString(DISCOUNT, "");

                GlobalVariables.al_items.clear();
                GlobalVariables.sub_total = "";
                GlobalVariables.total_discount = "";
                GlobalVariables.total_price = "";
                GlobalVariables.loyalty_discount_rate = "";
                GlobalVariables.loyalty_discount = "";
                GlobalVariables.vat = "";
                GlobalVariables.total_after_vat = "";
                GlobalVariables.net_price = "";

                Intent loginscreen=new Intent(CardPaymentActivity.this, LoginActivity.class);
                loginscreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginscreen);
                finish();
            }
        });

        //set spinner values
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.card));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_card_type.setAdapter(adapter);

        setData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GlobalVariables.al_items.size() > 0) {
            tv_cart_items.setVisibility(View.VISIBLE);
            tv_cart_items.setText(Integer.toString(GlobalVariables.al_items.size()));
        } else {
            tv_cart_items.setVisibility(View.GONE);
        }
    }

    private void setData() {
        tv_sub_total.setText(GlobalVariables.sub_total);
        tv_total_discount.setText(GlobalVariables.total_discount);
        tv_total_price.setText(GlobalVariables.total_price);
        tv_vat.setText(GlobalVariables.total_after_vat);
        tv_net_price.setText(GlobalVariables.net_price);

        if (!GlobalVariables.loyalty_discount.equals("")) {
            ll_loyalty.setVisibility(View.VISIBLE);
            tv_loyalty_lbl.setText("Loyalty  " + GlobalVariables.loyalty_discount_rate + "%");
            tv_loyalty.setText(GlobalVariables.loyalty_discount);
        }
    }

    private void saveData() {
        String url = BASE_URL + ADD_ORDER;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        usefullData.dismissProgress();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");

                            if (success.equals("1")) {
                                //clear arraylist
                                GlobalVariables.al_items.clear();

                                usefullData.showOrderSuccessfullDialog();

                                Log.e("Al size", "" + GlobalVariables.al_items.size());
                            }
                            usefullData.displayMessage(message);
                        } catch (JSONException e) {
                            Log.e("Add order", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        usefullData.dismissProgress();

                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }

                        Log.e("Error", error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Gson gson = new Gson();

                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", saveData.retreiveString(USER_ID));
                params.put("items", gson.toJson(GlobalVariables.al_items));
                params.put("tax", GlobalVariables.vat);
                params.put("method", "card");
                params.put("card_details", gson.toJson(al_card_details));
                params.put("paid", GlobalVariables.net_price);
                params.put("balance", "null");
                params.put("cus_id", cus_id);
                params.put("price", GlobalVariables.net_price);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void clearFields() {
        sp_card_type.setSelection(0);
        et_holder.setText("");
        et_card_num_4.setText("");

        tv_sub_total.setText("0.00");
        tv_total_discount.setText("0.00");
        tv_total_price.setText("0.00");
        tv_vat.setText("0.00");
        tv_net_price.setText("0.00");
    }
}
