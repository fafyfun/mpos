package com.example.chaminduchanaka.ramzijewelry;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.ramzijewelry.bean.ImportantDiscount;
import com.example.chaminduchanaka.ramzijewelry.service.GlobalVariables;
import com.example.chaminduchanaka.ramzijewelry.service.SaveData;
import com.example.chaminduchanaka.ramzijewelry.service.UsefullData;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.travijuu.numberpicker.library.Enums.ActionEnum;
import com.travijuu.numberpicker.library.Interface.ValueChangedListener;
import com.travijuu.numberpicker.library.NumberPicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.example.chaminduchanaka.ramzijewelry.service.Constance.ADD_LOYALTY;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.CHECK_USER_LEVEL;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.DISCOUNT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.GET_IMPORTANT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.HOLD;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.PASSWORD;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.SCAN_PRODUCT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.SEARCH_LOYALTY;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.TAX;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_ID;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_NAME;

public class MenuActivity extends AppCompatActivity {
    private ImageView iv_scan;
    private ImageView iv_search;
    private ImageView iv_logo;
    private ImageView iv_hold;

    private RelativeLayout rl_bill;
    private RelativeLayout rl_loyalty;
    private RelativeLayout rl_re_open;
    private RelativeLayout rl_logout;
    private RelativeLayout rl_discount;

    private TextView tv_cart_items;

    private RequestQueue requestQueue;
    private UsefullData usefullData;
    private SaveData saveData;

    private ArrayList<ImportantDiscount> al_important_discount;

    private double cashier_max_discount;

    private String user_id;
    private String user_level;
    private String discount;

    Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        requestQueue = Volley.newRequestQueue(this);
        usefullData = new UsefullData(this);
        saveData = new SaveData(this);

        tv_cart_items = findViewById(R.id.tv_cart_items);

        rl_bill = findViewById(R.id.rl_bill);
        rl_loyalty = findViewById(R.id.rl_loyalty);
        rl_re_open = findViewById(R.id.rl_re_open);
        rl_logout = findViewById(R.id.rl_log_out);
        rl_discount = findViewById(R.id.rl_discount);

        rl_discount.setEnabled(false);
        rl_re_open.setEnabled(false);

        iv_scan = findViewById(R.id.iv_scan);
        iv_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ScanActivity.class);
                startActivity(intent);
                finish();
            }
        });

        iv_search = findViewById(R.id.iv_search);
        iv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ProductListActivity.class);
                startActivity(intent);
                finish();
            }
        });

        iv_logo = findViewById(R.id.iv_logo);
        iv_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });

        iv_hold = findViewById(R.id.iv_hold);
        iv_hold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holdCart();
            }
        });
        iv_hold.setEnabled(false);

        rl_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, BillSummary.class);
                startActivity(intent);
            }
        });

        rl_loyalty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLoyaltyDialog();
            }
        });

        rl_re_open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ReOpenActivity.class);
                startActivity(intent);
            }
        });

        rl_discount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDiscountDialog();
            }
        });

        rl_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData.saveString(USER_NAME, "");
                saveData.saveString(PASSWORD, "");
                saveData.saveString(DISCOUNT, "");

                GlobalVariables.al_items.clear();
                GlobalVariables.sub_total = "";
                GlobalVariables.total_discount = "";
                GlobalVariables.total_price = "";
                GlobalVariables.loyalty_discount_rate = "";
                GlobalVariables.loyalty_discount = "";
                GlobalVariables.vat = "";
                GlobalVariables.total_after_vat = "";
                GlobalVariables.net_price = "";

                Intent loginscreen=new Intent(MenuActivity.this, LoginActivity.class);
                loginscreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginscreen);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GlobalVariables.al_items.size() > 0) {
            tv_cart_items.setVisibility(View.VISIBLE);
            tv_cart_items.setText(Integer.toString(GlobalVariables.al_items.size()));
        } else {
            tv_cart_items.setVisibility(View.GONE);
        }
    }

    private void holdCart() {
        if (GlobalVariables.al_items.size() != 0) {
            String url = BASE_URL + HOLD;
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Response", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");

                                if (success.equals("1")) {
                                    //clear arraylist
                                    GlobalVariables.al_items.clear();

                                    //clear number of the cart
                                    onResume();

                                    Log.e("Al size", "" + GlobalVariables.al_items.size());
                                }
                                usefullData.displayMessage(message);
                            } catch (JSONException e) {
                                Log.e("Add order", "Json Error");
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof NetworkError) {
                                usefullData.displayMessage("Network error occured..!");
                            } else if (error instanceof ServerError) {
                                usefullData.displayMessage("Network error occured..!");
                            } else if (error instanceof AuthFailureError) {
                                usefullData.displayMessage("Authentication failure..!");
                            } else if (error instanceof NoConnectionError) {
                                usefullData.displayMessage("No internet connection..!");
                            } else {
                                usefullData.displayMessage("Unknown error occured..!");
                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    Gson gson = new Gson();

                    params.put("user_id", saveData.retreiveString(USER_ID));
                    params.put("items", gson.toJson(GlobalVariables.al_items));
                    params.put("tax", "null");
                    params.put("method", "null");
                    params.put("card_details", "null");
                    params.put("paid", "null");
                    params.put("balance", "null");
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        } else {
            usefullData.displayMessage("Cart is empty");
        }
    }

    private void openLoyaltyDialog() {
        final Dialog dialog = new Dialog(this, R.style.dialog_theme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.loyality_dialog);

        final EditText et_loyalty_code = dialog.findViewById(R.id.et_loyalty_code);
        Button btn_done = dialog.findViewById(R.id.btn_done);
        Button btn_add_new = dialog.findViewById(R.id.btn_add_new);

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_loyalty_code.getText().toString().trim().equals("")) {
                    GlobalVariables.loyalty_discount = "";
                } else {
                    usefullData.showProgress("Please wait...");
                    //must check the loyality
                    String url = BASE_URL + SEARCH_LOYALTY;
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    usefullData.dismissProgress();
                                    Log.e("Loyalty response", response);
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String success = jsonObject.getString("success");
                                        if (success.equals("1")) {
                                            JSONArray jsonArray = jsonObject.getJSONArray("user");
                                            JSONObject user_obj = jsonArray.getJSONObject(0);
                                            if (user_obj.has("discount")) {
                                                //set loyalty discount rate
                                                GlobalVariables.loyalty_discount_rate = user_obj.getString("discount");
                                            }
                                            usefullData.displayMessage("Loyalty added");
                                        } else {
                                            GlobalVariables.loyalty_discount = "";
                                            String message = jsonObject.getString("message");
                                            usefullData.displayMessage(message);
                                        }
                                    } catch (JSONException e) {
                                        Log.e("Search loyalty", "Json Error");
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    usefullData.dismissProgress();

                                    if (error instanceof NetworkError) {
                                        usefullData.displayMessage("Network error occured..!");
                                    } else if (error instanceof ServerError) {
                                        usefullData.displayMessage("Network error occured..!");
                                    } else if (error instanceof AuthFailureError) {
                                        usefullData.displayMessage("Authentication failure..!");
                                    } else if (error instanceof NoConnectionError) {
                                        usefullData.displayMessage("No internet connection..!");
                                    } else {
                                        usefullData.displayMessage("Unknown error occured..!");
                                    }

                                    Log.e("Error", error.getMessage());
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("users", et_loyalty_code.getText().toString().trim());
                            return params;
                        }
                    };
                    requestQueue.add(stringRequest);
                }
                dialog.dismiss();
            }
        });
        btn_add_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddNewLoyaltyDialog();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void openAddNewLoyaltyDialog() {
        final Dialog dialog = new Dialog(this, R.style.dialog_theme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_new_loyalty_dialog);

        final EditText et_name = dialog.findViewById(R.id.et_name);
        final EditText et_contact = dialog.findViewById(R.id.et_contact);
        final EditText et_address = dialog.findViewById(R.id.et_address);
        final EditText et_email = dialog.findViewById(R.id.et_email);
        final EditText et_dob = dialog.findViewById(R.id.et_dob);
        Button btn_done = dialog.findViewById(R.id.btn_done);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "yyyy/MM/dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
                et_dob.setText(sdf.format(myCalendar.getTime()));
            }

        };

        et_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(MenuActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_name.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Name is required");
                } else if (et_contact.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Contact number is required");
                } else if (et_address.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Address is required");
                } else if (et_email.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Email is required");
                } else if (et_dob.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Date of birth is required");
                } else {
                    //save loyalty
                    usefullData.showProgress("Please wait...");
                    String url = BASE_URL + ADD_LOYALTY;
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    usefullData.dismissProgress();
                                    dialog.dismiss();
                                    Log.e("Loyalty add response", response);
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String message = jsonObject.getString("message");
                                        usefullData.displayMessage(message);

                                        dialog.dismiss();
                                    } catch (JSONException e) {
                                        Log.e("Search loyalty", "Json Error");
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    usefullData.dismissProgress();

                                    if (error instanceof NetworkError) {
                                        usefullData.displayMessage("Network error occured..!");
                                    } else if (error instanceof ServerError) {
                                        usefullData.displayMessage("Network error occured..!");
                                    } else if (error instanceof AuthFailureError) {
                                        usefullData.displayMessage("Authentication failure..!");
                                    } else if (error instanceof NoConnectionError) {
                                        usefullData.displayMessage("No internet connection..!");
                                    } else {
                                        usefullData.displayMessage("Unknown error occured..!");
                                    }

                                    Log.e("Error", error.getMessage());
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("name", et_name.getText().toString().trim());
                            params.put("contact_num", et_contact.getText().toString().trim());
                            params.put("address", et_address.getText().toString().trim());
                            params.put("dob", et_dob.getText().toString().trim());
                            params.put("email", et_email.getText().toString().trim());
                            return params;
                        }
                    };
                    requestQueue.add(stringRequest);
                }
            }
        });

        dialog.show();
    }

    private void openDiscountDialog() {
        getImportantDiscunt();

        final Dialog dialog = new Dialog(this, R.style.dialog_theme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.cart_item_dialog);

        final NumberPicker et_qty = dialog.findViewById(R.id.number_picker);
        final EditText et_discount = dialog.findViewById(R.id.et_discount);
        final EditText et_password = dialog.findViewById(R.id.et_password);
        final LinearLayout ll_qty = dialog.findViewById(R.id.ll_qty);
        Button btn_done = dialog.findViewById(R.id.btn_done);
        ImageView iv_close = dialog.findViewById(R.id.iv_close);

        ll_qty.setVisibility(View.GONE);

        if (!saveData.retreiveString(DISCOUNT).equals("")) {
            et_discount.setText(saveData.retreiveString(DISCOUNT));
        }

        et_discount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    et_password.setEnabled(true);
                } else {
                    et_password.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!et_discount.getText().toString().trim().equals("")) {
                    // has value to discount

                    //get cashier max discount
                    for (int i = 0; i < al_important_discount.size(); i++) {
                        if (al_important_discount.get(i).getName().equals("cashier_discount")) {
                            cashier_max_discount = Double.parseDouble(al_important_discount.get(i).getValue());
                        }
                    }

                    //entered discount
                    double entered_discount = Double.parseDouble(et_discount.getText().toString().trim());

                    if (et_password.getText().toString().trim().equals("")) {
                        if (entered_discount <= cashier_max_discount) {
                            saveData.saveString(DISCOUNT, et_discount.getText().toString().trim());
                            dialog.dismiss();
                        } else {
                            usefullData.displayMessage("Please enter password");
                        }

                    } else {
                        usefullData.showProgress("Please wait...");
                        String url = BASE_URL + CHECK_USER_LEVEL;
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        usefullData.dismissProgress();
                                        Log.e("User level response", response);
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            String success = jsonObject.getString("success");

                                            if (success.equals("1")) {
                                                JSONArray jsonArray = jsonObject.getJSONArray("user");
                                                JSONObject user_jObj = jsonArray.getJSONObject(0);
                                                if (user_jObj.has("user_id")) {
                                                    user_id = user_jObj.getString("user_id");
                                                }

                                                if (user_jObj.has("user_level")) {
                                                    user_level = user_jObj.getString("user_level");
                                                }


                                                //do process
                                                discount = et_discount.getText().toString().trim();

                                                //get max discount of the user
                                                double max_discount = 0;
                                                for (int i = 0; i < al_important_discount.size(); i++) {
                                                    if (al_important_discount.get(i).getName().equals(user_level)) {
                                                        max_discount = Double.parseDouble(al_important_discount.get(i).getValue());
                                                    }
                                                }

                                                Log.e("Max discount ", "" + max_discount);

                                                if ((Double.parseDouble(discount) <= max_discount)) {
                                                    saveData.saveString(DISCOUNT, discount);
                                                    dialog.dismiss();
                                                } else {
                                                    usefullData.displayMessage("Invalid discount");
                                                }


                                            } else {
                                                String message = jsonObject.getString("message");
                                                usefullData.displayMessage(message);
                                            }

                                            usefullData.dismissProgress();

                                        } catch (JSONException e) {
                                            Log.e("Check user level", "Json Error");
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        usefullData.dismissProgress();

                                        if (error instanceof NetworkError) {
                                            usefullData.displayMessage("Network error occured..!");
                                        } else if (error instanceof ServerError) {
                                            usefullData.displayMessage("Network error occured..!");
                                        } else if (error instanceof AuthFailureError) {
                                            usefullData.displayMessage("Authentication failure..!");
                                        } else if (error instanceof NoConnectionError) {
                                            usefullData.displayMessage("No internet connection..!");
                                        } else {
                                            usefullData.displayMessage("Unknown error occured..!");
                                        }

                                        Log.e("Error", error.getMessage());
                                    }
                                }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("code", et_password.getText().toString().trim());
                                return params;
                            }
                        };
                        requestQueue.add(stringRequest);
                    }
                } else {
                    //discount hasn't value

                    saveData.saveString(DISCOUNT, "");
                    dialog.dismiss();
                }
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void getImportantDiscunt() {

        al_important_discount = new ArrayList<>();

        String url = BASE_URL + GET_IMPORTANT;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Imoprtant", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                //get user details
                                JSONArray jsonArray = jsonObject.getJSONArray("important");
                                JSONArray jsonArray1 = jsonArray.getJSONArray(0);

                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject productJobj = jsonArray1.getJSONObject(i);
                                    ImportantDiscount obj = new ImportantDiscount();

                                    if (productJobj.has("id")) {
                                        obj.setId(productJobj.getString("id"));
                                    }

                                    if (productJobj.has("name")) {
                                        obj.setName(productJobj.getString("name"));
                                    }

                                    if (productJobj.has("value")) {
                                        obj.setValue(productJobj.getString("value"));
                                    }

//                                    if (productJobj.has("password")) {
//                                        obj.setPassword(productJobj.getString("password"));
//                                    }

                                    al_important_discount.add(obj);
                                }

                            } else {
                                String message = jsonObject.getString("message");
                                usefullData.displayMessage(message);
                            }
                        } catch (JSONException e) {
                            Log.e("getProductList()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }

                        Log.e("Error", error.getMessage());
                    }
                }) {
        };
        requestQueue.add(stringRequest);
    }
}
