package com.example.chaminduchanaka.ramzijewelry;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.ramzijewelry.adapter.BillItemsAdapter;
import com.example.chaminduchanaka.ramzijewelry.bean.ImportantDiscount;
import com.example.chaminduchanaka.ramzijewelry.bean.Item;
import com.example.chaminduchanaka.ramzijewelry.service.GlobalVariables;
import com.example.chaminduchanaka.ramzijewelry.service.SaveData;
import com.example.chaminduchanaka.ramzijewelry.service.UsefullData;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.travijuu.numberpicker.library.Enums.ActionEnum;
import com.travijuu.numberpicker.library.Interface.ValueChangedListener;
import com.travijuu.numberpicker.library.NumberPicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.example.chaminduchanaka.ramzijewelry.service.Constance.ADD_LOYALTY;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.ADD_ORDER;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.ADD_STOCK;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.CHECK_STOCK;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.CHECK_USER_LEVEL;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.DISCOUNT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.GET_IMPORTANT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.PASSWORD;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.SEARCH_LOYALTY;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.TAX;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_ID;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_NAME;

public class BillSummary extends AppCompatActivity {
    private Button btn_process;
    private Button btn_menu;
    private Button btn_back;
    private ListView lv_cart;
    private TextView tv_sub_total;
    private TextView tv_discount;
    private TextView tv_total_price;
    private TextView tv_vat;
    private TextView tv_net_price;
    private TextView tv_cart_items;
    private TextView tv_vat_lbl;
    private TextView tv_loyalty_lbl;
    private TextView tv_loyalty;

    private RelativeLayout rl_bill;
    private RelativeLayout rl_logout;

    private LinearLayout ll_loyalty;

    private ImageView iv_logo;

    private UsefullData usefullData;
    private RequestQueue requestQueue;
    private SaveData saveData;

    private String discount;
    private String qty;
    private String item_id;
    private String user_id;
    private String user_level;
    private String item_price;
    private String cus_id = "";

    private double cashier_max_discount;
    double paid;
    double balance;
    double loyalty_discount;

    Item obj;

    int stock;

    private ArrayList<ImportantDiscount> al_important_discount;

    Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_summary);

        usefullData = new UsefullData(this);
        requestQueue = Volley.newRequestQueue(this);
        saveData = new SaveData(this);

        btn_process = findViewById(R.id.btn_process);
        btn_menu = findViewById(R.id.btn_menu);
        btn_back = findViewById(R.id.btn_back);

        tv_sub_total = findViewById(R.id.tv_sub_total);
        tv_discount = findViewById(R.id.tv_discount);
        tv_total_price = findViewById(R.id.tv_total_price);
        tv_vat = findViewById(R.id.tv_vat);
        tv_net_price = findViewById(R.id.tv_net_price);
        tv_cart_items = findViewById(R.id.tv_cart_items);
        tv_vat_lbl = findViewById(R.id.tv_vat_lbl);
        tv_loyalty_lbl = findViewById(R.id.tv_loyalty_lbl);
        tv_loyalty = findViewById(R.id.tv_loyalty);

        rl_bill = findViewById(R.id.rl_bill);

        ll_loyalty = findViewById(R.id.ll_loyalty);

        rl_logout = findViewById(R.id.rl_log_out);

        lv_cart = findViewById(R.id.lv_cart);

        iv_logo = findViewById(R.id.iv_logo);

        tv_vat_lbl.setText("TAX  " + saveData.retreiveString(TAX) + "%");

        btn_process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GlobalVariables.loyalty_discount_rate.equals("")) {
                    openLoyaltyDialog();
                } else {
                    calculateBillTotal();
                    openPaymentDialog();
                }
            }
        });

        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BillSummary.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        iv_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BillSummary.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        lv_cart.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                item_id = GlobalVariables.al_items.get(position).getId();
                item_price = GlobalVariables.al_items.get(position).getSub_total();
                String qty = GlobalVariables.al_items.get(position).getQty();
                openDiscountDialog(qty);

                checkStock();
            }
        });

        rl_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData.saveString(USER_NAME, "");
                saveData.saveString(PASSWORD, "");
                saveData.saveString(DISCOUNT, "");

                GlobalVariables.al_items.clear();
                GlobalVariables.sub_total = "";
                GlobalVariables.total_discount = "";
                GlobalVariables.total_price = "";
                GlobalVariables.loyalty_discount_rate = "";
                GlobalVariables.loyalty_discount = "";
                GlobalVariables.vat = "";
                GlobalVariables.total_after_vat = "";
                GlobalVariables.net_price = "";

                Intent loginscreen=new Intent(BillSummary.this, LoginActivity.class);
                loginscreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginscreen);
                finish();
            }
        });

        getBillItems();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GlobalVariables.al_items.size() > 0) {
            tv_cart_items.setVisibility(View.VISIBLE);
            tv_cart_items.setText(Integer.toString(GlobalVariables.al_items.size()));
        } else {
            tv_cart_items.setVisibility(View.GONE);
            btn_process.setEnabled(false);
        }

        if (!GlobalVariables.loyalty_discount_rate.equals("")) {
            loyalty_discount = Double.parseDouble(GlobalVariables.loyalty_discount_rate);
            ll_loyalty.setVisibility(View.VISIBLE);
            tv_loyalty_lbl.setText("Loyalty  " + loyalty_discount + "%");
            calculateBillTotal();
        }
    }

    private void openPaymentDialog() {
        final Dialog dialog = new Dialog(this, R.style.dialog_theme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.payment_dialog);

        Button btn_cash = (Button) dialog.findViewById(R.id.btn_cash);
        Button btn_card = (Button) dialog.findViewById(R.id.btn_card);
        Button btn_hybrid = (Button) dialog.findViewById(R.id.btn_hybrid);
        Button btn_multi_card = (Button) dialog.findViewById(R.id.btn_multi_card);
        Button btn_cheque = (Button) dialog.findViewById(R.id.btn_cheque);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        btn_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialog.dismiss();
                    openCashPaymentDialog();
                } catch (Exception e) {}
            }
        });

        btn_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialog.dismiss();
                    Intent intent = new Intent(BillSummary.this, CardPaymentActivity.class);
                    intent.putExtra("cus_id", cus_id);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {}
            }
        });

        btn_hybrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialog.dismiss();
                    Intent intent = new Intent(BillSummary.this, HybridPaymentActivity.class);
                    intent.putExtra("cus_id", cus_id);
                    startActivity(intent);
                } catch (Exception e) {}
            }
        });

        btn_multi_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialog.dismiss();
                    Intent intent = new Intent(BillSummary.this, MultiCardActivity.class);
                    intent.putExtra("cus_id", cus_id);
                    startActivity(intent);
                } catch (Exception e) {}
            }
        });

        btn_cheque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialog.dismiss();
                    Intent intent = new Intent(BillSummary.this, ChequePaymentActivity.class);
                    intent.putExtra("cus_id", cus_id);
                    startActivity(intent);
                } catch (Exception e) {}
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void openCashPaymentDialog() {
        final Dialog dialog = new Dialog(this, R.style.dialog_theme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.cash_payment_dialog);

        Button btn_done = dialog.findViewById(R.id.btn_done);
        ImageView iv_close = dialog.findViewById(R.id.iv_close);
        TextView tv_net_price = dialog.findViewById(R.id.tv_net_price);
        final EditText et_paid = dialog.findViewById(R.id.et_paid);
        final EditText et_balance = dialog.findViewById(R.id.et_balance);

        tv_net_price.setText("Rs." + GlobalVariables.net_price);

        et_paid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (et_paid.getText().toString().length() > 0){
                    paid = Double.parseDouble(et_paid.getText().toString().trim());
                    double net_price = Double.parseDouble(GlobalVariables.net_price);

                    balance = paid - net_price;

                    et_balance.setText("Rs." + usefullData.formatNumber(balance));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (et_paid.getText().toString().trim().equals("")) {
                        usefullData.displayMessage("Fields cannot be empty");
                    } if (Double.parseDouble(GlobalVariables.net_price) > Double.parseDouble(et_paid.getText().toString().trim())) {
                        usefullData.displayMessage("Invalid amount");
                    } else {
                        usefullData.showProgress("Please wait...");
                        Log.e("Data submitted", "Success");
                        submitDataToServer("Cash");
                        dialog.dismiss();
                    }
                } catch (Exception e) {
                    usefullData.displayMessage("Fields cannot be empty");
                }
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void getBillItems() {
        lv_cart.setVisibility(View.VISIBLE);
        BillItemsAdapter adapter = new BillItemsAdapter(GlobalVariables.al_items, this);
        lv_cart.setAdapter(adapter);

        calculateBillTotal();
    }

    public void calculateBillTotal() {
        double sub_total = 0;
        for (int i = 0; i < GlobalVariables.al_items.size(); i++) {
            sub_total += Double.parseDouble(GlobalVariables.al_items.get(i).getPrice());
        }
        tv_sub_total.setText(usefullData.formatNumber(sub_total));

        double discount = 0;
        for (int i = 0; i < GlobalVariables.al_items.size(); i++) {
            discount += Double.parseDouble(GlobalVariables.al_items.get(i).getDiscount_total());
        }
        if (discount == 0) {
            tv_discount.setText("0.00");
        } else {
            tv_discount.setText(usefullData.formatNumber(discount));
        }

        double total_price = sub_total - discount;
        if (!GlobalVariables.loyalty_discount_rate.equals("")) {
            double loyalty_discount_tot = (total_price * loyalty_discount) / 100;
            tv_loyalty.setText(usefullData.formatNumber(loyalty_discount_tot));
            GlobalVariables.loyalty_discount = usefullData.formatNumber(loyalty_discount_tot);
            total_price -= loyalty_discount_tot;
        }
        tv_total_price.setText(usefullData.formatNumber(total_price));

        double vat = (total_price * Double.parseDouble(saveData.retreiveString(TAX))) / 100;
        tv_vat.setText(usefullData.formatNumber(vat));

        double net_price = total_price + vat;
        tv_net_price.setText(usefullData.formatNumber(net_price));

        //put values to global variables
        GlobalVariables.sub_total = tv_sub_total.getText().toString().trim();
        GlobalVariables.total_discount = tv_discount.getText().toString().trim();
        GlobalVariables.total_price = tv_total_price.getText().toString().trim();
        GlobalVariables.vat = saveData.retreiveString(TAX);
        GlobalVariables.total_after_vat = tv_vat.getText().toString().trim();
        GlobalVariables.net_price = tv_net_price.getText().toString().trim();
    }

    int val;
    private void openDiscountDialog(String q) {
        getImportantDiscunt();

        obj = new Item();

        final Dialog dialog = new Dialog(this, R.style.dialog_theme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.cart_item_dialog);

        final NumberPicker et_qty = dialog.findViewById(R.id.number_picker);
        final EditText et_discount = dialog.findViewById(R.id.et_discount);
        final EditText et_password = dialog.findViewById(R.id.et_password);
        Button btn_done = dialog.findViewById(R.id.btn_done);
        ImageView iv_close = dialog.findViewById(R.id.iv_close);

        et_qty.setValue(Integer.parseInt(q));

        val = et_qty.getValue();

        et_qty.setValueChangedListener(new ValueChangedListener() {
            @Override
            public void valueChanged(int value, ActionEnum action) {

                if (value > stock) {
                    et_qty.setValue(stock);
                    usefullData.displayMessage("Out of stock");
                } else  {
                    if (value < val) {
//                        addStock("+1");
                    } else {
//                        addStock("-1");
                    }

                    val = et_qty.getValue();
                }
            }
        });

        et_discount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    et_password.setEnabled(true);
                } else {
                    et_password.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_qty.getValue() == 0) {
                    usefullData.displayMessage("Please enter quantity");
                } else if (!et_discount.getText().toString().trim().equals("")) {
                    // has value to discount

                    //get cashier max discount
                    for (int i = 0; i < al_important_discount.size(); i++) {
                        if (al_important_discount.get(i).getName().equals("cashier_discount")) {
                            cashier_max_discount = Double.parseDouble(al_important_discount.get(i).getValue());
                        }
                    }

                    //get current discount of the product
                    double current_discount = 0;
                    for (int i = 0; i < GlobalVariables.al_items.size(); i++) {
                        if (GlobalVariables.al_items.get(i).getId().equals(item_id)) {
                            current_discount = Double.parseDouble(GlobalVariables.al_items.get(i).getDiscount());
                        }
                    }

                    //entered discount
                    double entered_discount = Double.parseDouble(et_discount.getText().toString().trim());

                    if (et_password.getText().toString().trim().equals("")) {
                        if (entered_discount <= cashier_max_discount) {
                            discount = et_discount.getText().toString().trim();
                            qty = Integer.toString(et_qty.getValue());
                            calculateLineTotal();
                            dialog.dismiss();
                        } else {
                            usefullData.displayMessage("Please enter password");
                        }

                    } else {
                        usefullData.showProgress("Please wait...");
                        String url = BASE_URL + CHECK_USER_LEVEL;
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        usefullData.dismissProgress();
                                        Log.e("User level response", response);
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            String success = jsonObject.getString("success");

                                            if (success.equals("1")) {
                                                JSONArray jsonArray = jsonObject.getJSONArray("user");
                                                JSONObject user_jObj = jsonArray.getJSONObject(0);
                                                if (user_jObj.has("user_id")) {
                                                    user_id = user_jObj.getString("user_id");
                                                }

                                                if (user_jObj.has("user_level")) {
                                                    user_level = user_jObj.getString("user_level");
                                                }


                                                //do process
                                                qty = Integer.toString(et_qty.getValue());
                                                discount = et_discount.getText().toString().trim();

                                                //get current discount of the product
                                                double current_discount = 0;
                                                for (int i = 0; i < GlobalVariables.al_items.size(); i++) {
                                                    if (GlobalVariables.al_items.get(i).getId().equals(item_id)) {
                                                        current_discount = Double.parseDouble(GlobalVariables.al_items.get(i).getDiscount());
                                                    }
                                                }


                                                //get max discount of the user
                                                double max_discount = 0;
                                                for (int i = 0; i < al_important_discount.size(); i++) {
                                                    if (al_important_discount.get(i).getName().equals(user_level)) {
                                                        max_discount = Double.parseDouble(al_important_discount.get(i).getValue());
                                                    }
                                                }

                                                Log.e("Max discount ", "" + max_discount);
                                                Log.e("Current discount ", "" + current_discount);

                                                if ((current_discount <= Double.parseDouble(discount)) && (Double.parseDouble(discount) <= max_discount)) {
                                                    calculateLineTotal();
                                                    dialog.dismiss();
                                                } else {
                                                    usefullData.displayMessage("Invalid discount");
                                                }


                                            } else {
                                                String message = jsonObject.getString("message");
                                                usefullData.displayMessage(message);
                                            }

                                            usefullData.dismissProgress();

                                        } catch (JSONException e) {
                                            Log.e("Check user level", "Json Error");
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        usefullData.dismissProgress();

                                        if (error instanceof NetworkError) {
                                            usefullData.displayMessage("Network error occured..!");
                                        } else if (error instanceof ServerError) {
                                            usefullData.displayMessage("Network error occured..!");
                                        } else if (error instanceof AuthFailureError) {
                                            usefullData.displayMessage("Authentication failure..!");
                                        } else if (error instanceof NoConnectionError) {
                                            usefullData.displayMessage("No internet connection..!");
                                        } else {
                                            usefullData.displayMessage("Unknown error occured..!");
                                        }

                                        Log.e("Error", error.getMessage());
                                    }
                                }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("code", et_password.getText().toString().trim());
                                return params;
                            }
                        };
                        requestQueue.add(stringRequest);
                    }
                } else {
                    //discount hasn't value only change qty
                    qty = Integer.toString(et_qty.getValue());

                    //get item discount
                    for (int i = 0; i < GlobalVariables.al_items.size(); i++) {
                        if (GlobalVariables.al_items.get(i).getId().equals(item_id)) {
                            discount = GlobalVariables.al_items.get(i).getDiscount();
                            break;
                        }
                    }

                    calculateLineTotal();
                    dialog.dismiss();
                }
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void getImportantDiscunt() {

        al_important_discount = new ArrayList<>();

        String url = BASE_URL + GET_IMPORTANT;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Imoprtant", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                //get user details
                                JSONArray jsonArray = jsonObject.getJSONArray("important");
                                JSONArray jsonArray1 = jsonArray.getJSONArray(0);

                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject productJobj = jsonArray1.getJSONObject(i);
                                    ImportantDiscount obj = new ImportantDiscount();

                                    if (productJobj.has("id")) {
                                        obj.setId(productJobj.getString("id"));
                                    }

                                    if (productJobj.has("name")) {
                                        obj.setName(productJobj.getString("name"));
                                    }

                                    if (productJobj.has("value")) {
                                        obj.setValue(productJobj.getString("value"));
                                    }

//                                    if (productJobj.has("password")) {
//                                        obj.setPassword(productJobj.getString("password"));
//                                    }

                                    al_important_discount.add(obj);
                                }

                            } else {
                                String message = jsonObject.getString("message");
                                usefullData.displayMessage(message);
                            }
                        } catch (JSONException e) {
                            Log.e("getProductList()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }

                        Log.e("Error", error.getMessage());
                    }
                }) {
        };
        requestQueue.add(stringRequest);
    }

    private void calculateLineTotal() {
        for (int i = 0; i < GlobalVariables.al_items.size(); i++) {
            if (GlobalVariables.al_items.get(i).getId().equals(item_id)) {
                //get item price
                int item_price = Integer.parseInt(GlobalVariables.al_items.get(i).getItem_price());

                //set item id
                obj.setId(item_id);

                //set item name
                obj.setName(GlobalVariables.al_items.get(i).getName());

                //set new quantity
                int new_qty = Integer.parseInt(qty);
                obj.setQty(qty);

                //set new discount
                double new_discount = Double.parseDouble(discount);
                obj.setDiscount(Double.toString(new_discount));

                //set new price total of prices
                int tot_price = item_price * new_qty;
                obj.setPrice(Integer.toString(tot_price));

                //set new discount total
                double total_discount = 0;
                if (!discount.equals("0")) {
                    total_discount = (tot_price * new_discount) / 100;
                }
                obj.setDiscount_total(Double.toString(total_discount));

                //set new sub total
                double new_sub_total = tot_price - total_discount;
                obj.setSub_total(Double.toString(new_sub_total));

                //set item price
                obj.setItem_price(GlobalVariables.al_items.get(i).getItem_price());

                GlobalVariables.al_items.remove(i);

                GlobalVariables.al_items.add(obj);

                getBillItems();

                break;
            }
        }
    }

    private void submitDataToServer(final String method) {
        String url = BASE_URL + ADD_ORDER;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        usefullData.dismissProgress();
                        Log.e("Add order response", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");

                            if (success.equals("1")) {
                                //clear arraylist
                                GlobalVariables.al_items.clear();
                                lv_cart.invalidateViews();

                                usefullData.showOrderSuccessfullDialog();

                                Log.e("Al size", "" + GlobalVariables.al_items.size());
                            }
                            usefullData.displayMessage(message);
                            Log.e("Message", message);
                        } catch (JSONException e) {
                            Log.e("Add order", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        usefullData.dismissProgress();

                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }

                        Log.e("Error", error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //add arraylist to json array
                Gson gson = new Gson();

                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", saveData.retreiveString(USER_ID));
                params.put("items", gson.toJson(GlobalVariables.al_items));
                params.put("tax", GlobalVariables.vat);
                params.put("method", method);
                params.put("card_details", "null");
                params.put("paid", Double.toString(paid));
                params.put("balance", Double.toString(balance));
                params.put("cus_id", cus_id);
                params.put("price", GlobalVariables.net_price);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void openLoyaltyDialog() {
        final Dialog dialog = new Dialog(this, R.style.dialog_theme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.loyality_dialog);

        final EditText et_loyalty_code = dialog.findViewById(R.id.et_loyalty_code);
        Button btn_done = dialog.findViewById(R.id.btn_done);
        Button btn_add_new = dialog.findViewById(R.id.btn_add_new);
        ImageView iv_close = dialog.findViewById(R.id.iv_close);

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_loyalty_code.getText().toString().trim().equals("")) {
                    GlobalVariables.loyalty_discount = "";
                    openPaymentDialog();
                } else {
                    usefullData.showProgress("Please wait...");
                    //must check the loyality
                    String url = BASE_URL + SEARCH_LOYALTY;
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    usefullData.dismissProgress();
                                    Log.e("Loyalty response", response);
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String success = jsonObject.getString("success");
                                        if (success.equals("1")) {
                                            JSONArray jsonArray = jsonObject.getJSONArray("user");
                                            JSONObject user_obj = jsonArray.getJSONObject(0);
                                            if (user_obj.has("discount")) {
                                                ll_loyalty.setVisibility(View.VISIBLE);

                                                //set loyalty discount rate
                                                loyalty_discount = Double.parseDouble(user_obj.getString("discount"));
                                                tv_loyalty_lbl.setText("Loyalty  " + user_obj.getString("discount") + "%");
                                                GlobalVariables.loyalty_discount_rate = user_obj.getString("discount");
                                                cus_id = user_obj.getString("cus_id");

                                                //calculate bill total
                                                calculateBillTotal();

                                                //open payment dialog
                                                openPaymentDialog();
                                            }
                                        } else {
                                            GlobalVariables.loyalty_discount = "";
                                            String message = jsonObject.getString("message");
                                            usefullData.displayMessage(message);
                                        }
                                    } catch (JSONException e) {
                                        Log.e("Search loyalty", "Json Error");
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    usefullData.dismissProgress();

                                    if (error instanceof NetworkError) {
                                        usefullData.displayMessage("Network error occured..!");
                                    } else if (error instanceof ServerError) {
                                        usefullData.displayMessage("Network error occured..!");
                                    } else if (error instanceof AuthFailureError) {
                                        usefullData.displayMessage("Authentication failure..!");
                                    } else if (error instanceof NoConnectionError) {
                                        usefullData.displayMessage("No internet connection..!");
                                    } else {
                                        usefullData.displayMessage("Unknown error occured..!");
                                    }

                                    Log.e("Error", error.getMessage());
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("users", et_loyalty_code.getText().toString().trim());
                            return params;
                        }
                    };
                    requestQueue.add(stringRequest);
                }
                dialog.dismiss();
            }
        });
        btn_add_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddNewLoyaltyDialog();
                dialog.dismiss();
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void openAddNewLoyaltyDialog() {
        final Dialog dialog = new Dialog(this, R.style.dialog_theme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_new_loyalty_dialog);

        final EditText et_name = dialog.findViewById(R.id.et_name);
        final EditText et_contact = dialog.findViewById(R.id.et_contact);
        final EditText et_address = dialog.findViewById(R.id.et_address);
        final EditText et_email = dialog.findViewById(R.id.et_email);
        final EditText et_dob = dialog.findViewById(R.id.et_dob);
        Button btn_done = dialog.findViewById(R.id.btn_done);
        ImageView iv_close = dialog.findViewById(R.id.iv_close);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "yyyy/MM/dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
                et_dob.setText(sdf.format(myCalendar.getTime()));
            }

        };

        et_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(BillSummary.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (et_name.getText().toString().trim().equals("")) {
                        usefullData.displayMessage("Name is required");
                    } else if (et_contact.getText().toString().trim().equals("")) {
                        usefullData.displayMessage("Contact number is required");
                    } else {
                        //save loyalty
                        usefullData.showProgress("Please wait...");
                        String url = BASE_URL + ADD_LOYALTY;
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        usefullData.dismissProgress();
                                        openPaymentDialog();
                                        dialog.dismiss();
                                        Log.e("Loyalty add response", response);
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            String success = jsonObject.getString("success");
                                            String message = jsonObject.getString("message");
                                            cus_id = jsonObject.getString("cus_id");
                                            usefullData.displayMessage(message);

                                            dialog.dismiss();
                                        } catch (JSONException e) {
                                            Log.e("Search loyalty", "Json Error");
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        usefullData.dismissProgress();

//                                    if (error instanceof NetworkError) {
//                                        usefullData.displayMessage("Network error occured..!");
//                                    } else if (error instanceof ServerError) {
//                                        usefullData.displayMessage("Network error occured..!");
//                                    } else if (error instanceof AuthFailureError) {
//                                        usefullData.displayMessage("Authentication failure..!");
//                                    } else if (error instanceof NoConnectionError) {
//                                        usefullData.displayMessage("No internet connection..!");
//                                    } else {
//                                        usefullData.displayMessage("Unknown error occured..!");
//                                    }
                                    }
                                }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("name", et_name.getText().toString().trim());
                                params.put("contact_num", et_contact.getText().toString().trim());
                                params.put("address", et_address.getText().toString().trim());
                                params.put("dob", et_dob.getText().toString().trim());
                                params.put("email", et_email.getText().toString().trim());
                                return params;
                            }
                        };
                        requestQueue.add(stringRequest);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void addStock(final String value) {
        String url = BASE_URL + ADD_STOCK;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Add Stock Response", response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", item_id);
                params.put("price", item_price);
                params.put("size", "null");
                params.put("stock", value);
                params.put("user", saveData.retreiveString(USER_ID));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void checkStock() {
        String url = BASE_URL + CHECK_STOCK;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Check Stock Response", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                //get product details
                                JSONArray jsonArray = jsonObject.getJSONArray("product_stock");
                                JSONObject product = jsonArray.getJSONObject(0);

                                if (product.has("stock")) {
                                    stock = Integer.parseInt(product.getString("stock"));

                                    Log.e("Stock", "" + stock);
                                }

                            } else {
                                String message = jsonObject.getString("message");
                                usefullData.displayMessage(message);
                            }
                        } catch (JSONException e) {
                            Log.e("checkLogin()", "Json Error");
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

//                        if (error instanceof NetworkError) {
//                            usefullData.displayMessage("Network error occured..!");
//                        } else if (error instanceof ServerError) {
//                            usefullData.displayMessage("Network error occured..!");
//                        } else if (error instanceof AuthFailureError) {
//                            usefullData.displayMessage("Authentication failure..!");
//                        } else if (error instanceof NoConnectionError) {
//                            usefullData.displayMessage("No internet connection..!");
//                        } else {
//                            usefullData.displayMessage("Unknown error occured..!");
//                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", item_id);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

}
