package com.example.chaminduchanaka.ramzijewelry;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bugsnag.android.Bugsnag;
import com.example.chaminduchanaka.ramzijewelry.bean.ImportantDiscount;
import com.example.chaminduchanaka.ramzijewelry.service.SaveData;
import com.example.chaminduchanaka.ramzijewelry.service.UsefullData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.chaminduchanaka.ramzijewelry.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.FULL_NAME;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.GET_IMPORTANT;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.LOGIN;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.PASSWORD;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.TAX;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_ID;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_NAME;
import static com.example.chaminduchanaka.ramzijewelry.service.Constance.USER_TYPE;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private UsefullData usefullData;
    private RequestQueue requestQueue;
    private SaveData saveData;

    private EditText et_userName;
    private EditText et_Password;

    private Button btn_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usefullData = new UsefullData(this);
        requestQueue = Volley.newRequestQueue(this);
        saveData = new SaveData(this);

        et_userName = findViewById(R.id.et_userName);
        et_Password = findViewById(R.id.et_Password);

        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);

        Bugsnag.init(this);

        //check shared preferance values
        if (!(saveData.retreiveString(USER_NAME).equals("") && saveData.retreiveString(PASSWORD).equals(""))) {
            //start main activity
            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
            startActivity(intent);
            finish();

            getImportantDiscunt();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                if (et_userName.getText().toString().trim().equals("") || et_Password.getText().toString().trim().equals("")) {
                    usefullData.displayMessage("Fields cannot be empty");
                } else {
                    checkLogin();
                    getImportantDiscunt();
                }

        }
    }

    private void checkLogin() {
        String url = BASE_URL + LOGIN;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            Log.e("Success", success);
                            if (success.equals("1")) {
                                //get user details
                                JSONArray jsonArray = jsonObject.getJSONArray("user");
                                JSONObject user = jsonArray.getJSONObject(0);

                                if (user.has("user_username")) {
                                    saveData.saveString(FULL_NAME, user.getString("user_username"));
                                }

                                if (user.has("user_id")) {
                                    saveData.saveString(USER_ID, user.getString("user_id"));
                                }

                                if (user.has("user_type")) {
                                    saveData.saveString(USER_TYPE, user.getString("user_type"));
                                }

                                //start main activity
                                Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                                startActivity(intent);
                                finish();

                                //save data in shared preferance
                                saveData.saveString(USER_NAME, et_userName.getText().toString().trim());
                                saveData.saveString(PASSWORD, et_Password.getText().toString().trim());
                            } else {
                                String message = jsonObject.getString("message");
                                usefullData.displayMessage(message);
                            }
                        } catch (JSONException e) {
                            Log.e("checkLogin()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("users", et_userName.getText().toString().trim());
                params.put("password", et_Password.getText().toString().trim());
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void getImportantDiscunt() {

        String url = BASE_URL + GET_IMPORTANT;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Imoprtant", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                //get user details
                                JSONArray jsonArray = jsonObject.getJSONArray("important");
                                JSONArray jsonArray1 = jsonArray.getJSONArray(0);

                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject productJobj = jsonArray1.getJSONObject(i);

                                    if (productJobj.has("name")) {
                                        String val = (productJobj.getString("name"));
                                        if (val.equals("tax")) {
                                            if (productJobj.has("value")) {
                                                saveData.saveString(TAX, productJobj.getString("value"));
                                            }
                                        }
                                    }
                                }

                            } else {
                                String message = jsonObject.getString("message");
                                usefullData.displayMessage(message);
                            }
                        } catch (JSONException e) {
                            Log.e("getImportantDiscount()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMessage("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMessage("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMessage("No internet connection..!");
                        } else {
                            usefullData.displayMessage("Unknown error occured..!");
                        }
                    }
                }) {
        };
        requestQueue.add(stringRequest);
    }
}
