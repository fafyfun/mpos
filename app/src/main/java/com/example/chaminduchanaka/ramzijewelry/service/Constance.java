package com.example.chaminduchanaka.ramzijewelry.service;

public interface Constance {
//    String BASE_URL = "http:/192.168.1.61/mpos/";
    String BASE_URL = "https://mpos.bestsoftwareapps.com/";
    String LOGIN = "login.php";
    String SCAN_PRODUCT = "scanporduct.php";
    String PRODUCT_LIST = "product_list.php";
    String GET_IMPORTANT = "get_important.php";
    String ADD_ORDER = "add_order.php";
    String HOLD = "hold.php";
    String SEARCH_LOYALTY = "search_loyal.php";
    String CHECK_USER_LEVEL = "check_user_level.php";
    String ADD_LOYALTY = "add_loyal.php";
    String RE_OPEN = "reopen_list.php";
    String RE_OPEN_SELECT = "reopen_select.php";
    String ADD_STOCK = "add_stock.php";
    String CHECK_STOCK = "productstock.php";

    String USER_NAME = "username";
    String PASSWORD = "password";
    String FULL_NAME = "full_name";
    String USER_TYPE = "user_type";
    String USER_ID = "user_id";

    String TAX = "tax";
    String DISCOUNT = "discount";

    String MY_PREFS_NAME = "mpos";
}
