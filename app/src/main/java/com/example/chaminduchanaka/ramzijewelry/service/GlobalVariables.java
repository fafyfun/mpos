package com.example.chaminduchanaka.ramzijewelry.service;

import com.example.chaminduchanaka.ramzijewelry.bean.Item;

import java.util.ArrayList;

public class GlobalVariables {
    public static ArrayList<Item> al_items = new ArrayList<>();

    public static String sub_total;
    public static String total_discount;
    public static String total_price;
    public static String loyalty_discount_rate = "";
    public static String loyalty_discount = "";
    public static String vat;
    public static String total_after_vat;
    public static String net_price;
}
